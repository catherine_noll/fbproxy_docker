apimocker_is_running () {
    ps aux | grep -v grep | grep -q apimocker
}

until apimocker_is_running ; do sleep 1 ; done
