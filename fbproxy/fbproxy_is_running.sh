fbproxy_is_running () {
    ps aux | grep -v grep | grep -q fbproxy
}

until fbproxy_is_running ; do sleep 1 ; done
