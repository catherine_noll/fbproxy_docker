#!/bin/bash

docker-compose up -d redis 

docker-compose up -d apimocker

docker-compose up -d mysqldb

docker exec mysqldb /bin/bash /mysql_is_running.sh
docker exec mysqldb /bin/bash /initialize_database.sh

docker-compose up -d tokenaudit

docker-compose up -d fbproxy
docker exec mysqldb /bin/bash /fbproxy_is_running.sh
