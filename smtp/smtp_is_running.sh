smtp_is_running () {
    ps aux | grep -v grep | grep -q smtp
}

until smtp_is_running ; do sleep 1 ; done
