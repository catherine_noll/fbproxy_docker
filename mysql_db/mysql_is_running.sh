mysql_is_running () {
    ps aux | grep -v grep | grep -q mysqld
}

until mysql_is_running ; do sleep 1 ; done
