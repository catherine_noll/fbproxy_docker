USE ampush_db1;

-- Dummy data that is currently queried by FB Proxy (see FB Proxy's bootstrap.properties file)
-- TO DO:  get real mysql DB running in Docker; or refactor FB Proxy to get contract list from Redis


INSERT INTO pl_contract
(
id, name, client_id, contracttype_id, fee, acquisitiontype_id, sdate, edate, iototal
)
VALUES 
(
"1392", "Ampush Recruiting", 96, 5, 0.1000, 1, "2015-01-11 00:00:00", "2017-06-28 00:00:00", 0.0000
);


INSERT INTO facebook_account
(
id, fbid, created_on, updated_on, retired_on, name, timezone, access_token
)
VALUES 
(
"2271", "act_345926612250332", "2014-09-19 12:55:50", "2015-10-10 08:18:48", "2100-12-31 23:59:59", "Ampush_Careers", 1, "CAACcXbmWPQMBAMyfPrywPC5ZBhjfW64f1kDXQwfiSB3ZCJdpJTLLAFK0HAGjgU0TK152Y4TrZCuzdjgSO8z8zbE1RR1rmKiRzbm4waYXDI4JgRiM603PXLB8UbRgvEY3tZB27si5mc6rviwAnhUr9x7e19dbDMwqqKxOmap4HcajHDxhLULid2rPDSZAvHgwNrskGjPwF5wp44ZCGRqlKR"
);


INSERT INTO pl_contractaccountmap
(
id, name, contract_id, account_id
)
VALUES 
(
"1", "Ampush Recruiting", "1392", "2271"
);


-- Token Audit configuration (see https://ampush.atlassian.net/wiki/display/AIRU/Tokens+Audit+Trail)

ALTER TABLE `facebook_tokensaudittrail`;
ALTER TABLE `facebook_tokensaudittrail`;
CREATE INDEX `facebook_tokensaudittrail_94eaf5d5` ON `facebook_tokensaudittrail` (`event_ts`);
CREATE INDEX `facebook_tokensaudittrail_2be07fce` ON `facebook_tokensaudittrail` (`event_type`);
CREATE INDEX `facebook_tokensaudittrail_bfac9f99` ON `facebook_tokensaudittrail` (`token`);
CREATE INDEX `facebook_tokensaudittrail_6f2fe10e` ON `facebook_tokensaudittrail` (`account_id`);
CREATE INDEX `facebook_tokensaudittrail_b07bb6ae` ON `facebook_tokensaudittrail` (`contract_id`);

INSERT INTO facebook_tokensaudittrail
(
insert_ts, event_ts, event_type, token_type, token, account_id, contract_id, amp_response_key, fb_api_response, proxy_server
)
VALUES 
(
"2014-10-25 17:01:40", "1412049575165", "MISSING", "", "", "2271", "1392", "", "", "proxy1.ampushinsight.com" 
);