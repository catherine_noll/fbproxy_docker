-- MySQL dump 10.13  Distrib 5.6.19, for Linux (x86_64)
--
-- Host: localhost    Database: ampush_db1
-- ------------------------------------------------------
-- Server version	5.6.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account_acl`
--
USE ampush_db1;

DROP TABLE IF EXISTS `account_acl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_acl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `category` varchar(45) NOT NULL,
  `subject` varchar(50) NOT NULL,
  `items` longtext NOT NULL,
  `sdate` datetime NOT NULL,
  `edate` datetime NOT NULL,
  `createP` tinyint(1) NOT NULL,
  `readP` tinyint(1) NOT NULL,
  `updateP` tinyint(1) NOT NULL,
  `deleteP` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_refs_id_99ba7081` (`user_id`),
  KEY `group_id_refs_id_d9728ae0` (`group_id`),
  CONSTRAINT `group_id_refs_id_d9728ae0` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `user_id_refs_id_99ba7081` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=594 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `account_apicredentials`
--

DROP TABLE IF EXISTS `account_apicredentials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_apicredentials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  `service` varchar(50) NOT NULL,
  `api_key` varchar(256) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `profile_id` (`profile_id`,`service`),
  CONSTRAINT `profile_id_refs_id_c0cfff09` FOREIGN KEY (`profile_id`) REFERENCES `account_profile` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=530 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `account_credential`
--

DROP TABLE IF EXISTS `account_credential`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_credential` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service` varchar(50) CHARACTER SET latin1 NOT NULL,
  `login` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tags` varchar(1000) DEFAULT NULL,
  `password` varchar(250) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `account_credential_f774835d` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=2462 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `account_profile`
--

DROP TABLE IF EXISTS `account_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `bm_linked` tinyint(1) NOT NULL DEFAULT '0',
  `bm_role` varchar(20) NOT NULL DEFAULT '',
  `fbid` varchar(50) NOT NULL DEFAULT '',
  `fb_email` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `user_id_refs_id_b2a790` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=572 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `account_profile_credentials`
--

DROP TABLE IF EXISTS `account_profile_credentials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_profile_credentials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  `credential_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `profile_id` (`profile_id`,`credential_id`),
  KEY `account_profile_credentials_141c6eec` (`profile_id`),
  KEY `account_profile_credentials_8d2e3fcd` (`credential_id`),
  CONSTRAINT `credential_id_refs_id_a7dfac3d` FOREIGN KEY (`credential_id`) REFERENCES `account_credential` (`id`),
  CONSTRAINT `profile_id_refs_id_eb30aa30` FOREIGN KEY (`profile_id`) REFERENCES `account_profile` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ad_creator_ad_validation_map`
--

DROP TABLE IF EXISTS `ad_creator_ad_validation_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_creator_ad_validation_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order` int(11) NOT NULL,
  `validation_node` varchar(50) NOT NULL,
  `creative_type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ad_creator_bulk_job`
--

DROP TABLE IF EXISTS `ad_creator_bulk_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_creator_bulk_job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext NOT NULL,
  `job_type` varchar(25) NOT NULL,
  `user_id` int(11) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `next_action` varchar(30) NOT NULL,
  `celery_job_id` int(11) NOT NULL,
  `workflow_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `total_pend` int(11) DEFAULT '0',
  `total_succ` int(11) DEFAULT '0',
  `total_fail` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `workflow_id_refs_id_c9b128d2` (`workflow_id`),
  KEY `user_id_refs_id_1bf299b6` (`user_id`),
  KEY `account_id_refs_id_a7ddbd3f` (`account_id`),
  CONSTRAINT `account_id_refs_id_a7ddbd3f` FOREIGN KEY (`account_id`) REFERENCES `facebook_account` (`id`),
  CONSTRAINT `user_id_refs_id_1bf299b6` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `workflow_id_refs_id_c9b128d2` FOREIGN KEY (`workflow_id`) REFERENCES `ad_creator_workflow` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67372 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ad_creator_bulk_job_details`
--

DROP TABLE IF EXISTS `ad_creator_bulk_job_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_creator_bulk_job_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_number` int(11) NOT NULL,
  `ad_type` varchar(40) DEFAULT NULL,
  `ad_type_num` int(11) NOT NULL,
  `content` longtext NOT NULL,
  `status` varchar(20) NOT NULL,
  `errors` longtext,
  `bulk_job_id` int(11) NOT NULL,
  `validated_fields` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `invalidated_fields` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `parent_campaign_id` int(11) DEFAULT NULL,
  `parent_set_id` int(11) DEFAULT NULL,
  `level` char(20) NOT NULL DEFAULT 'campaign',
  PRIMARY KEY (`id`),
  KEY `bulk_job_id_refs_id_9680d52c` (`bulk_job_id`),
  KEY `parent_campaign_id_refs_id_c241b1a5` (`parent_campaign_id`),
  KEY `parent_set_id_refs_id_c241b1a5` (`parent_set_id`),
  CONSTRAINT `bulk_job_id_refs_id_9680d52c` FOREIGN KEY (`bulk_job_id`) REFERENCES `ad_creator_bulk_job` (`id`),
  CONSTRAINT `parent_campaign_id_refs_id_c241b1a5` FOREIGN KEY (`parent_campaign_id`) REFERENCES `ad_creator_bulk_job_details` (`id`),
  CONSTRAINT `parent_set_id_refs_id_c241b1a5` FOREIGN KEY (`parent_set_id`) REFERENCES `ad_creator_bulk_job_details` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30473555 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ad_creator_img_bank`
--

DROP TABLE IF EXISTS `ad_creator_img_bank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_creator_img_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `hash` longtext NOT NULL,
  `url` longtext NOT NULL,
  `account_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `account_id_refs_id_c3a92233` (`account_id`),
  CONSTRAINT `account_id_refs_id_c3a92233` FOREIGN KEY (`account_id`) REFERENCES `facebook_account` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16783 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ad_creator_locales`
--

DROP TABLE IF EXISTS `ad_creator_locales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_creator_locales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` int(11) NOT NULL,
  `name` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ad_creator_states_countries`
--

DROP TABLE IF EXISTS `ad_creator_states_countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_creator_states_countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=244 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ad_creator_workflow`
--

DROP TABLE IF EXISTS `ad_creator_workflow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_creator_workflow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37027 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ad_creator_workflow_adgroups`
--

DROP TABLE IF EXISTS `ad_creator_workflow_adgroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ad_creator_workflow_adgroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_id` int(11) NOT NULL,
  `adgroup_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `workflow_id` (`workflow_id`,`adgroup_id`),
  KEY `adgroup_id_refs_id_4375256` (`adgroup_id`),
  CONSTRAINT `adgroup_id_refs_id_4375256` FOREIGN KEY (`adgroup_id`) REFERENCES `facebook_adgroup` (`id`),
  CONSTRAINT `workflow_id_refs_id_aebfeaa2` FOREIGN KEY (`workflow_id`) REFERENCES `ad_creator_workflow` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6131775 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `analytics_pagevisitrecord`
--

DROP TABLE IF EXISTS `analytics_pagevisitrecord`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `analytics_pagevisitrecord` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `visit_date` datetime NOT NULL,
  `url` varchar(200) NOT NULL,
  `params` varchar(200) NOT NULL,
  `extra_data` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_refs_id_9b977629` (`user_id`),
  CONSTRAINT `user_id_refs_id_9b977629` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30057 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `audiences_audience`
--

DROP TABLE IF EXISTS `audiences_audience`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audiences_audience` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fbid` bigint(20) NOT NULL,
  `account_id` int(11) NOT NULL,
  `approximate_count` bigint(20) NOT NULL,
  `sent_count` bigint(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` varchar(500) NOT NULL,
  `parent_audience_id` int(11) DEFAULT NULL,
  `parent_category` varchar(32) NOT NULL,
  `status` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `type_name` varchar(32) NOT NULL,
  `subtype` int(11) NOT NULL,
  `subtype_name` varchar(32) NOT NULL,
  `opt_out_url` varchar(512) NOT NULL,
  `rule` varchar(8192) NOT NULL,
  `retention_days` int(11) DEFAULT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `fb_updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `fbid` (`fbid`),
  KEY `account_id_refs_id_51c6d563` (`account_id`),
  KEY `parent_audience_id_refs_id_e9c2783` (`parent_audience_id`),
  CONSTRAINT `account_id_refs_id_51c6d563` FOREIGN KEY (`account_id`) REFERENCES `facebook_account` (`id`),
  CONSTRAINT `parent_audience_id_refs_id_e9c2783` FOREIGN KEY (`parent_audience_id`) REFERENCES `audiences_audience` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=168547 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_bda51c3c` (`group_id`),
  KEY `auth_group_permissions_1e014c8f` (`permission_id`),
  CONSTRAINT `group_id_refs_id_3cea63fe` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `permission_id_refs_id_a7792de1` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=409 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_message`
--

DROP TABLE IF EXISTS `auth_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `message` longtext CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_message_fbfc09f1` (`user_id`),
  CONSTRAINT `user_id_refs_id_9af0b65a` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_e4470c6e` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_728de91f` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=304 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) CHARACTER SET latin1 NOT NULL,
  `first_name` varchar(30) CHARACTER SET latin1 NOT NULL,
  `last_name` varchar(30) CHARACTER SET latin1 NOT NULL,
  `email` varchar(75) CHARACTER SET latin1 NOT NULL,
  `password` varchar(128) CHARACTER SET latin1 NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=622 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_fbfc09f1` (`user_id`),
  KEY `auth_user_groups_bda51c3c` (`group_id`),
  CONSTRAINT `group_id_refs_id_f0ee9890` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `user_id_refs_id_831107f1` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=395 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_fbfc09f1` (`user_id`),
  KEY `auth_user_user_permissions_1e014c8f` (`permission_id`),
  CONSTRAINT `permission_id_refs_id_67e79cb` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `user_id_refs_id_f2045483` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=694 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `celery_taskmeta`
--

DROP TABLE IF EXISTS `celery_taskmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `celery_taskmeta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` varchar(255) NOT NULL,
  `status` varchar(50) NOT NULL,
  `result` longtext,
  `date_done` datetime NOT NULL,
  `traceback` longtext,
  `hidden` tinyint(1) NOT NULL,
  `meta` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_id` (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `celery_tasksetmeta`
--

DROP TABLE IF EXISTS `celery_tasksetmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `celery_tasksetmeta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskset_id` varchar(255) NOT NULL,
  `result` longtext NOT NULL,
  `date_done` datetime NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `taskset_id` (`taskset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `consolidation_result`
--

DROP TABLE IF EXISTS `consolidation_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consolidation_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL,
  `sdate` datetime NOT NULL,
  `edate` datetime NOT NULL,
  `format` varchar(45) NOT NULL,
  `subject` varchar(1000) NOT NULL,
  `item` varchar(1000) NOT NULL,
  `comparison_engine` varchar(255) NOT NULL,
  `checked` smallint(5) unsigned NOT NULL,
  `failed` smallint(5) unsigned NOT NULL,
  `discrepancies` varchar(10000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `consolidation_result_sdate` (`sdate`),
  KEY `consolidation_result_edate` (`edate`),
  KEY `consolidation_result_timestamp` (`timestamp`)
) ENGINE=InnoDB AUTO_INCREMENT=109572 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conversion_affiliate`
--

DROP TABLE IF EXISTS `conversion_affiliate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conversion_affiliate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aff` varchar(30) NOT NULL,
  `aff_cat` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conversion_conversion`
--

DROP TABLE IF EXISTS `conversion_conversion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conversion_conversion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `session_id` varchar(50) NOT NULL,
  `ip` char(15) NOT NULL,
  `src_id` int(11) DEFAULT NULL,
  `aff_id` int(11) DEFAULT NULL,
  `adid` varchar(20) NOT NULL,
  `uid` varchar(20) NOT NULL,
  `event` varchar(20) NOT NULL,
  `spent` decimal(12,2) DEFAULT NULL,
  `convtype` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30983 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conversion_conversioncounts`
--

DROP TABLE IF EXISTS `conversion_conversioncounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conversion_conversioncounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `conversion` int(11) NOT NULL,
  `hits` int(11) NOT NULL,
  `conv_level` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30983 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conversion_conversiondebugger`
--

DROP TABLE IF EXISTS `conversion_conversiondebugger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conversion_conversiondebugger` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL,
  `sdatetime` datetime NOT NULL,
  `edatetime` datetime NOT NULL,
  `account_id` int(11) NOT NULL,
  `contract_id` int(11) NOT NULL,
  `contract_ref` varchar(100) DEFAULT NULL,
  `stats_ref` varchar(100) DEFAULT NULL,
  `asid` varchar(100) DEFAULT NULL,
  `status` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `conversion_conversiondebugger_67f1b7ce` (`timestamp`),
  KEY `conversion_conversiondebugger_7561f0de` (`sdatetime`),
  KEY `conversion_conversiondebugger_197f6670` (`edatetime`),
  KEY `conversion_conversiondebugger_6f2fe10e` (`account_id`),
  KEY `conversion_conversiondebugger_4f844952` (`contract_id`),
  KEY `conversion_conversiondebugger_51e14211` (`contract_ref`),
  KEY `conversion_conversiondebugger_407da091` (`stats_ref`),
  KEY `conversion_conversiondebugger_60bf5c34` (`asid`),
  KEY `conversion_conversiondebugger_36528e23` (`status`),
  CONSTRAINT `account_id_refs_id_1b6b565c` FOREIGN KEY (`account_id`) REFERENCES `facebook_account` (`id`),
  CONSTRAINT `contract_id_refs_id_2447381e` FOREIGN KEY (`contract_id`) REFERENCES `pl_contract` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23753 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conversion_privatekey`
--

DROP TABLE IF EXISTS `conversion_privatekey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conversion_privatekey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pkey` varchar(20) NOT NULL,
  `hash_func` varchar(6) NOT NULL,
  `contact_specific_ref` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conversion_source`
--

DROP TABLE IF EXISTS `conversion_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conversion_source` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `src` varchar(30) DEFAULT NULL,
  `src_cat` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1977 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_fbfc09f1` (`user_id`),
  KEY `django_admin_log_e4470c6e` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_288599e6` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `user_id_refs_id_c8665aa` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10349 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `app_label` varchar(100) CHARACTER SET latin1 NOT NULL,
  `model` varchar(100) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) CHARACTER SET latin1 NOT NULL,
  `session_data` longtext CHARACTER SET latin1 NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_c25c2c28` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `django_site`
--

DROP TABLE IF EXISTS `django_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) CHARACTER SET latin1 NOT NULL,
  `name` varchar(50) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `djcelery_crontabschedule`
--

DROP TABLE IF EXISTS `djcelery_crontabschedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_crontabschedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `minute` varchar(64) NOT NULL,
  `hour` varchar(64) NOT NULL,
  `day_of_week` varchar(64) NOT NULL,
  `day_of_month` varchar(64) NOT NULL,
  `month_of_year` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `djcelery_intervalschedule`
--

DROP TABLE IF EXISTS `djcelery_intervalschedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_intervalschedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `every` int(11) NOT NULL,
  `period` varchar(24) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `djcelery_periodictask`
--

DROP TABLE IF EXISTS `djcelery_periodictask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_periodictask` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `task` varchar(200) NOT NULL,
  `interval_id` int(11) DEFAULT NULL,
  `crontab_id` int(11) DEFAULT NULL,
  `args` longtext NOT NULL,
  `kwargs` longtext NOT NULL,
  `queue` varchar(200) DEFAULT NULL,
  `exchange` varchar(200) DEFAULT NULL,
  `routing_key` varchar(200) DEFAULT NULL,
  `expires` datetime DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `last_run_at` datetime DEFAULT NULL,
  `total_run_count` int(10) unsigned NOT NULL,
  `date_changed` datetime NOT NULL,
  `description` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `interval_id_refs_id_f2054349` (`interval_id`),
  KEY `crontab_id_refs_id_ebff5e74` (`crontab_id`),
  CONSTRAINT `crontab_id_refs_id_ebff5e74` FOREIGN KEY (`crontab_id`) REFERENCES `djcelery_crontabschedule` (`id`),
  CONSTRAINT `interval_id_refs_id_f2054349` FOREIGN KEY (`interval_id`) REFERENCES `djcelery_intervalschedule` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `djcelery_periodictasks`
--

DROP TABLE IF EXISTS `djcelery_periodictasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_periodictasks` (
  `ident` smallint(6) NOT NULL,
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`ident`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `djcelery_taskstate`
--

DROP TABLE IF EXISTS `djcelery_taskstate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_taskstate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(64) NOT NULL,
  `task_id` varchar(36) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `tstamp` datetime NOT NULL,
  `args` longtext,
  `kwargs` longtext,
  `eta` datetime DEFAULT NULL,
  `expires` datetime DEFAULT NULL,
  `result` longtext,
  `traceback` longtext,
  `runtime` double DEFAULT NULL,
  `retries` int(11) NOT NULL,
  `worker_id` int(11) DEFAULT NULL,
  `hidden` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_id` (`task_id`),
  KEY `worker_id_refs_id_4e3453a` (`worker_id`),
  CONSTRAINT `worker_id_refs_id_4e3453a` FOREIGN KEY (`worker_id`) REFERENCES `djcelery_workerstate` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `djcelery_workerstate`
--

DROP TABLE IF EXISTS `djcelery_workerstate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_workerstate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(255) NOT NULL,
  `last_heartbeat` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hostname` (`hostname`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_account`
--

DROP TABLE IF EXISTS `facebook_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fbid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `retired_on` datetime NOT NULL,
  `qstatus` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `credential_id` int(11) DEFAULT NULL,
  `timezone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `access_token` longtext COLLATE utf8_unicode_ci,
  `token_status` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Account Timezone name',
  `status_id` int(11) DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `daily_spend_limit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spend_cap` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `handshake_completed` tinyint(1) NOT NULL DEFAULT '0',
  `synch_limit` int(11) NOT NULL DEFAULT '0',
  `bm_linked` tinyint(1) NOT NULL DEFAULT '0',
  `access_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `permitted_roles` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fb_created_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `facebook_unique_accounts` (`fbid`,`retired_on`),
  KEY `facebook_account_8995078c` (`fbid`),
  KEY `facebook_account_8d2e3fcd` (`credential_id`),
  KEY `facebook_account_name` (`name`),
  KEY `currency_id_refs_id_f6573736` (`currency_id`),
  KEY `status_id_refs_id_d6bf8fd` (`status_id`),
  CONSTRAINT `credential_id_refs_id_19411b78` FOREIGN KEY (`credential_id`) REFERENCES `account_credential` (`id`),
  CONSTRAINT `currency_id_refs_id_f6573736` FOREIGN KEY (`currency_id`) REFERENCES `facebook_currency` (`id`),
  CONSTRAINT `status_id_refs_id_d6bf8fd` FOREIGN KEY (`status_id`) REFERENCES `facebook_accountstatus` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2824 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_account_adgroups`
--

DROP TABLE IF EXISTS `facebook_account_adgroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_account_adgroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `adgroup_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_id` (`account_id`,`adgroup_id`),
  KEY `facebook_account_adgroups_6f2fe10e` (`account_id`),
  KEY `facebook_account_adgroups_f43940d3` (`adgroup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10789406 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_account_campaigngroups`
--

DROP TABLE IF EXISTS `facebook_account_campaigngroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_account_campaigngroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `campaigngroup_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_id` (`account_id`,`campaigngroup_id`),
  KEY `facebook_account_campaigngroup_6f2fe10e` (`account_id`),
  KEY `facebook_account_campaigngroup_8fd46b1a` (`campaigngroup_id`),
  CONSTRAINT `account_id_refs_id_2ff45eb2` FOREIGN KEY (`account_id`) REFERENCES `facebook_account` (`id`),
  CONSTRAINT `campaigngroup_id_refs_id_4cc55f66` FOREIGN KEY (`campaigngroup_id`) REFERENCES `facebook_campaigngroup` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=879265 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_account_campaigns`
--

DROP TABLE IF EXISTS `facebook_account_campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_account_campaigns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_id` (`account_id`,`campaign_id`),
  KEY `facebook_account_campaigns_6f2fe10e` (`account_id`),
  KEY `facebook_account_campaigns_8fd46b1a` (`campaign_id`),
  CONSTRAINT `account_id_refs_id_2ff45eb1` FOREIGN KEY (`account_id`) REFERENCES `facebook_account` (`id`),
  CONSTRAINT `campaign_id_refs_id_4cc55f65` FOREIGN KEY (`campaign_id`) REFERENCES `facebook_campaign` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12853780 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_account_capabilities`
--

DROP TABLE IF EXISTS `facebook_account_capabilities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_account_capabilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `accountcapability_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_id` (`account_id`,`accountcapability_id`),
  KEY `accountcapability_id_refs_id_d9d60523` (`accountcapability_id`),
  CONSTRAINT `accountcapability_id_refs_id_d9d60523` FOREIGN KEY (`accountcapability_id`) REFERENCES `facebook_accountcapability` (`id`),
  CONSTRAINT `account_id_refs_id_3158d56d` FOREIGN KEY (`account_id`) REFERENCES `facebook_account` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101856 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_account_users`
--

DROP TABLE IF EXISTS `facebook_account_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_account_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_id` (`account_id`,`user_id`),
  KEY `user_id_refs_id_7e314389` (`user_id`),
  CONSTRAINT `user_id_refs_id_7e314389` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9325 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_accountcapability`
--

DROP TABLE IF EXISTS `facebook_accountcapability`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_accountcapability` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_accountgroup`
--

DROP TABLE IF EXISTS `facebook_accountgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_accountgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fbid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `retired_on` datetime NOT NULL,
  `qstatus` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `group_number` int(11) DEFAULT NULL,
  `is_master` tinyint(1) NOT NULL DEFAULT '0',
  `is_full` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `facebook_unique_adgroupaccounts` (`fbid`,`retired_on`),
  KEY `facebook_accountgroup_search` (`fbid`,`retired_on`,`created_on`)
) ENGINE=InnoDB AUTO_INCREMENT=69302 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_accountgroup_accounts`
--

DROP TABLE IF EXISTS `facebook_accountgroup_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_accountgroup_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accountgroup_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `accountgroup_id` (`accountgroup_id`,`account_id`),
  KEY `facebook_accountgroup_accounts_af6b7a86` (`accountgroup_id`),
  KEY `facebook_accountgroup_accounts_6f2fe10e` (`account_id`),
  CONSTRAINT `accountgroup_id_refs_id_592952af` FOREIGN KEY (`accountgroup_id`) REFERENCES `facebook_accountgroup` (`id`),
  CONSTRAINT `account_id_refs_id_3b27d63d` FOREIGN KEY (`account_id`) REFERENCES `facebook_account` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3056 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_accountstats`
--

DROP TABLE IF EXISTS `facebook_accountstats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_accountstats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` datetime NOT NULL,
  `total` smallint(5) unsigned NOT NULL,
  `disapproved` smallint(5) unsigned NOT NULL,
  `pending_review` smallint(5) unsigned NOT NULL,
  `paused` smallint(5) unsigned NOT NULL,
  `active` smallint(5) unsigned NOT NULL,
  `deleted` smallint(5) unsigned NOT NULL,
  `campaigns` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_accountstatus`
--

DROP TABLE IF EXISTS `facebook_accountstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_accountstatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_ad`
--

DROP TABLE IF EXISTS `facebook_ad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_ad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `adSpecid` int(11) NOT NULL,
  `creative_spec` int(11) NOT NULL,
  `target_audience` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `facebook_ad_b1236e52` (`adSpecid`),
  KEY `facebook_ad_27d0d65d` (`creative_spec`),
  KEY `facebook_ad_1de5d1f8` (`target_audience`),
  CONSTRAINT `_adSpecid_refs_id_c8a872ae` FOREIGN KEY (`adSpecid`) REFERENCES `facebook_adspec` (`id`),
  CONSTRAINT `_creative_spec_refs_id_aac218ac` FOREIGN KEY (`creative_spec`) REFERENCES `facebook_creativespec` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_adbody`
--

DROP TABLE IF EXISTS `facebook_adbody`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_adbody` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `body` varchar(135) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `body` (`body`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_adcreative`
--

DROP TABLE IF EXISTS `facebook_adcreative`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_adcreative` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(36) DEFAULT NULL,
  `image_hash` varchar(100) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `creative` varchar(125) DEFAULT NULL,
  `updated` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `image_hash` (`image_hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_adgroup`
--

DROP TABLE IF EXISTS `facebook_adgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_adgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fbid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `retired_on` datetime NOT NULL,
  `qstatus` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `ad_id` int(11) DEFAULT NULL,
  `update_count` int(11) DEFAULT '0',
  `tags` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `max_bid` double NOT NULL,
  `bid_type` int(11) NOT NULL,
  `base_type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `asid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_url` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `facebook_unique_adgroups` (`fbid`,`retired_on`),
  KEY `facebook_adgroup_8995078c` (`fbid`),
  KEY `facebook_adgroup_dd565239` (`created_on`),
  KEY `facebook_adgroup_c350a442` (`updated_on`),
  KEY `facebook_adgroup_51c59e2f` (`ad_id`),
  CONSTRAINT `ad_id_refs_id_80b826b3` FOREIGN KEY (`ad_id`) REFERENCES `facebook_ad` (`id`),
  CONSTRAINT `_ad_id_refs_id_80b826b3` FOREIGN KEY (`ad_id`) REFERENCES `facebook_ad` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=294335011 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_adheadline`
--

DROP TABLE IF EXISTS `facebook_adheadline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_adheadline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `headline` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `headline` (`headline`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_adspec`
--

DROP TABLE IF EXISTS `facebook_adspec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_adspec` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `objectType` int(11) NOT NULL,
  `body_id` int(11) DEFAULT NULL,
  `headline_id` int(11) DEFAULT NULL,
  `creative_id` int(11) DEFAULT NULL,
  `url` varchar(1000) NOT NULL,
  `link_url` varchar(1000) NOT NULL,
  `asid` varchar(50) DEFAULT NULL,
  `url_tags` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `facebook_adspec_5b892844` (`body_id`),
  KEY `facebook_adspec_1a38ac4` (`headline_id`),
  KEY `facebook_adspec_2bc6c40f` (`creative_id`),
  KEY `facebook_adspec_60bf5c34` (`asid`),
  CONSTRAINT `body_id_refs_id_f54c6bf8` FOREIGN KEY (`body_id`) REFERENCES `facebook_adbody` (`id`),
  CONSTRAINT `creative_id_refs_id_a59e0731` FOREIGN KEY (`creative_id`) REFERENCES `facebook_adcreative` (`id`),
  CONSTRAINT `headline_id_refs_id_d37a20f0` FOREIGN KEY (`headline_id`) REFERENCES `facebook_adheadline` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_btcategory`
--

DROP TABLE IF EXISTS `facebook_btcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_btcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fbid` varchar(25) NOT NULL,
  `name` varchar(100) NOT NULL,
  `parent` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `fbid` (`fbid`)
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_campaign`
--

DROP TABLE IF EXISTS `facebook_campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_campaign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fbid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `retired_on` datetime NOT NULL,
  `qstatus` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `max_bid` double NOT NULL,
  `bid_type` int(11) NOT NULL,
  `base_type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_time` datetime NOT NULL,
  `stop_time` datetime DEFAULT NULL,
  `daily_budget` double NOT NULL,
  `lifetime_budget` double NOT NULL,
  `update_count` int(11) DEFAULT '0',
  `tags` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `facebook_unique_campaigns` (`fbid`,`retired_on`),
  KEY `facebook_campaign_8995078c` (`fbid`),
  KEY `facebook_campaign_c350a442` (`updated_on`),
  KEY `facebook_campaign_search` (`fbid`,`retired_on`,`created_on`)
) ENGINE=InnoDB AUTO_INCREMENT=1408535631 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_campaign_adgroups`
--

DROP TABLE IF EXISTS `facebook_campaign_adgroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_campaign_adgroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) NOT NULL,
  `adgroup_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `campaign_id` (`campaign_id`,`adgroup_id`),
  KEY `facebook_campaign_adgroups_8fd46b1a` (`campaign_id`),
  KEY `facebook_campaign_adgroups_f43940d3` (`adgroup_id`),
  CONSTRAINT `adgroup_id_refs_id_809199d3` FOREIGN KEY (`adgroup_id`) REFERENCES `facebook_adgroup` (`id`),
  CONSTRAINT `campaign_id_refs_id_da9d833e` FOREIGN KEY (`campaign_id`) REFERENCES `facebook_campaign` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10792796 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_campaigngroup`
--

DROP TABLE IF EXISTS `facebook_campaigngroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_campaigngroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fbid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `tags` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `budget` double NOT NULL,
  `objective` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `retired_on` datetime NOT NULL,
  `qstatus` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `facebook_unique_campaigngroup` (`fbid`),
  KEY `facebook_campaigngroup_8995078c` (`fbid`),
  KEY `facebook_campaigngroup_c350a442` (`updated_on`),
  KEY `facebook_campaigngroup_search` (`fbid`,`retired_on`,`created_on`)
) ENGINE=InnoDB AUTO_INCREMENT=879327 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_campaigngroup_campaigns`
--

DROP TABLE IF EXISTS `facebook_campaigngroup_campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_campaigngroup_campaigns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaigngroup_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `campaigngroup_id` (`campaigngroup_id`,`campaign_id`),
  KEY `facebook_campaigngroup_campaigns_8fd46b1b` (`campaigngroup_id`),
  KEY `facebook_campaigngroup_campaigns_f43940d4` (`campaign_id`),
  CONSTRAINT `campaigngroup_id_refs_id_80b826b5` FOREIGN KEY (`campaigngroup_id`) REFERENCES `facebook_campaigngroup` (`id`),
  CONSTRAINT `campaign_id_refs_id_da9d833f` FOREIGN KEY (`campaign_id`) REFERENCES `facebook_campaign` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9020370 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_creativespec`
--

DROP TABLE IF EXISTS `facebook_creativespec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_creativespec` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fbid` varchar(50) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `retired_on` datetime NOT NULL,
  `qstatus` varchar(1) DEFAULT NULL,
  `ad_creative` int(11) DEFAULT NULL,
  `ad_body` int(11) DEFAULT NULL,
  `ad_headline` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `fbid` (`fbid`,`retired_on`),
  KEY `facebook_creativespec_7ab203c3` (`ad_creative`),
  KEY `facebook_creativespec_ace0c17a` (`ad_body`),
  KEY `facebook_creativespec_6451233a` (`ad_headline`),
  CONSTRAINT `ad_body_refs_id_812b0c46` FOREIGN KEY (`ad_body`) REFERENCES `facebook_adbody` (`id`),
  CONSTRAINT `ad_creative_refs_id_700c285d` FOREIGN KEY (`ad_creative`) REFERENCES `facebook_adcreative` (`id`),
  CONSTRAINT `ad_headline_refs_id_195294be` FOREIGN KEY (`ad_headline`) REFERENCES `facebook_adheadline` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31284405 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_currency`
--

DROP TABLE IF EXISTS `facebook_currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `code` varchar(5) NOT NULL,
  `offset` int(11) NOT NULL,
  `symbol` varchar(3) DEFAULT NULL,
  `group` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_etag`
--

DROP TABLE IF EXISTS `facebook_etag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_etag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(100) NOT NULL,
  `timestamp` datetime NOT NULL,
  `etag` varchar(100) NOT NULL,
  `paging` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=95883 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_locale`
--

DROP TABLE IF EXISTS `facebook_locale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_locale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `locale` (`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_page`
--

DROP TABLE IF EXISTS `facebook_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fbid` varchar(50) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `access_token` longtext NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `access_type` varchar(50) NOT NULL DEFAULT '',
  `permitted_roles` varchar(255) NOT NULL DEFAULT '',
  `category` varchar(100) NOT NULL DEFAULT '',
  `bm_linked` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=326 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_pixel`
--

DROP TABLE IF EXISTS `facebook_pixel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_pixel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fbid` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `tag` varchar(150) NOT NULL,
  `status` varchar(150) NOT NULL,
  `account_id` int(11) NOT NULL,
  `js_pixel` longtext,
  `value` int(11) DEFAULT NULL,
  `tracking_spec` varchar(255) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `currency` varchar(3) NOT NULL,
  `time_created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `facebook_pixel_6f2fe10e` (`account_id`),
  KEY `facebook_pixel_fbfc09f1` (`user_id`),
  CONSTRAINT `account_id_refs_id_3a77e8bc` FOREIGN KEY (`account_id`) REFERENCES `facebook_account` (`id`),
  CONSTRAINT `user_id_refs_id_ebda737f` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=782 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_pixel_accounts`
--

DROP TABLE IF EXISTS `facebook_pixel_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_pixel_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pixel_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pixel_id` (`pixel_id`,`account_id`),
  KEY `account_id_refs_id_7f2731f4` (`account_id`),
  CONSTRAINT `account_id_refs_id_7f2731f4` FOREIGN KEY (`account_id`) REFERENCES `facebook_account` (`id`),
  CONSTRAINT `pixel_id_refs_id_7b38ebe3` FOREIGN KEY (`pixel_id`) REFERENCES `facebook_pixel` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3697 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_reportspec`
--

DROP TABLE IF EXISTS `facebook_reportspec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_reportspec` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `start_time` datetime NOT NULL,
  `stop_time` datetime NOT NULL,
  `report_type` varchar(15) NOT NULL,
  `summarize_by` varchar(15) NOT NULL,
  `aggregation_time` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_reportspec_filter_adgroups`
--

DROP TABLE IF EXISTS `facebook_reportspec_filter_adgroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_reportspec_filter_adgroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reportspec_id` int(11) NOT NULL,
  `adgroup_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `reportspec_id` (`reportspec_id`,`adgroup_id`),
  KEY `facebook_reportspec_filter_adgroups_302280e1` (`reportspec_id`),
  KEY `facebook_reportspec_filter_adgroups_f43940d3` (`adgroup_id`),
  CONSTRAINT `adgroup_id_refs_id_734c5018` FOREIGN KEY (`adgroup_id`) REFERENCES `facebook_adgroup` (`id`),
  CONSTRAINT `reportspec_id_refs_id_f1880582` FOREIGN KEY (`reportspec_id`) REFERENCES `facebook_reportspec` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_reportspec_filter_campaigns`
--

DROP TABLE IF EXISTS `facebook_reportspec_filter_campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_reportspec_filter_campaigns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reportspec_id` int(11) NOT NULL,
  `campaign_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `reportspec_id` (`reportspec_id`,`campaign_id`),
  KEY `facebook_reportspec_filter_campaigns_302280e1` (`reportspec_id`),
  KEY `facebook_reportspec_filter_campaigns_8fd46b1a` (`campaign_id`),
  CONSTRAINT `campaign_id_refs_id_f8e4509a` FOREIGN KEY (`campaign_id`) REFERENCES `facebook_campaign` (`id`),
  CONSTRAINT `reportspec_id_refs_id_42432ed7` FOREIGN KEY (`reportspec_id`) REFERENCES `facebook_reportspec` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_request`
--

DROP TABLE IF EXISTS `facebook_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` longtext NOT NULL,
  `api_method` varchar(50) NOT NULL,
  `http_method` varchar(10) NOT NULL,
  `params` longtext NOT NULL,
  `attempts` smallint(5) unsigned NOT NULL,
  `max_attempts` smallint(5) unsigned NOT NULL,
  `priority` smallint(5) unsigned NOT NULL,
  `response` longtext,
  `status` varchar(25) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `queued` datetime NOT NULL,
  `last_tried` datetime DEFAULT NULL,
  `completed` datetime DEFAULT NULL,
  `time_used` varchar(30) DEFAULT NULL,
  `fb_time_used` varchar(30) DEFAULT NULL,
  `object_id` varchar(50) DEFAULT NULL,
  `object_class` varchar(50) DEFAULT NULL,
  `owner` varchar(50) DEFAULT NULL,
  `req_id` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `facebook_request_e7867c17` (`api_method`),
  KEY `facebook_request_468caf84` (`http_method`),
  KEY `facebook_request_c9ad71dd` (`status`),
  KEY `facebook_request_e94d8f76` (`visible`),
  KEY `facebook_request_1771444a` (`queued`),
  KEY `facebook_request_829e37fd` (`object_id`),
  KEY `facebook_request_4741fd1b` (`owner`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_requestlocalservice`
--

DROP TABLE IF EXISTS `facebook_requestlocalservice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_requestlocalservice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` longtext NOT NULL,
  `api_method` varchar(50) NOT NULL,
  `http_method` varchar(10) NOT NULL,
  `params` longtext NOT NULL,
  `attempts` smallint(5) unsigned NOT NULL,
  `max_attempts` smallint(5) unsigned NOT NULL,
  `priority` smallint(5) unsigned NOT NULL,
  `response` longtext,
  `status` varchar(25) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `queued` datetime NOT NULL,
  `last_tried` datetime DEFAULT NULL,
  `completed` datetime DEFAULT NULL,
  `time_used` varchar(30) DEFAULT NULL,
  `fb_time_used` varchar(30) DEFAULT NULL,
  `object_id` varchar(50) DEFAULT NULL,
  `object_class` varchar(50) DEFAULT NULL,
  `owner` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_schedulespec`
--

DROP TABLE IF EXISTS `facebook_schedulespec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_schedulespec` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `datetime` datetime NOT NULL,
  `time_next` datetime NOT NULL,
  `report_spec_id` int(11) NOT NULL,
  `frequency` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `facebook_schedulespec_842afc03` (`report_spec_id`),
  CONSTRAINT `report_spec_id_refs_id_178a3686` FOREIGN KEY (`report_spec_id`) REFERENCES `facebook_reportspec` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_targetingcategory`
--

DROP TABLE IF EXISTS `facebook_targetingcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_targetingcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `fbid` varchar(50) NOT NULL,
  `audience_size` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `path` longtext NOT NULL,
  `description` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `facebook_targetingcategory_52094d6e` (`name`),
  KEY `facebook_targetingcategory_8995078c` (`fbid`)
) ENGINE=InnoDB AUTO_INCREMENT=834 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_timezone`
--

DROP TABLE IF EXISTS `facebook_timezone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_timezone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timezone_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `group` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `facebook_tokensaudittrail`
--

DROP TABLE IF EXISTS `facebook_tokensaudittrail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_tokensaudittrail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `insert_ts` datetime NOT NULL,
  `event_ts` bigint(20) NOT NULL,
  `event_type` varchar(100) NOT NULL,
  `token_type` varchar(2) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `contract_id` int(11) DEFAULT NULL,
  `amp_response_key` varchar(150) DEFAULT NULL,
  `fb_api_response` longtext,
  `proxy_server` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `facebook_tokensaudittrail_94eaf5d5` (`event_ts`),
  KEY `facebook_tokensaudittrail_2be07fce` (`event_type`),
  KEY `facebook_tokensaudittrail_bfac9f99` (`token`),
  KEY `facebook_tokensaudittrail_6f2fe10e` (`account_id`),
  KEY `facebook_tokensaudittrail_b07bb6ae` (`contract_id`),
  CONSTRAINT `account_id_refs_id_657e4d1c` FOREIGN KEY (`account_id`) REFERENCES `facebook_account` (`id`),
  CONSTRAINT `contract_id_refs_id_e3c96396` FOREIGN KEY (`contract_id`) REFERENCES `pl_contract` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20363 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_parameter`
--

DROP TABLE IF EXISTS `manager_parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `id_field` varchar(255) NOT NULL,
  `id_value` int(11) NOT NULL,
  `source` varchar(45) NOT NULL,
  `meta` longtext NOT NULL,
  `order` int(11) NOT NULL,
  `equation` varchar(1024) DEFAULT NULL,
  `definition` varchar(1024) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_field` (`id_field`),
  KEY `category_id_refs_id_a75198a7` (`category_id`),
  CONSTRAINT `category_id_refs_id_a75198a7` FOREIGN KEY (`category_id`) REFERENCES `manager_parametercategory` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=364661386 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_parameter_contracts`
--

DROP TABLE IF EXISTS `manager_parameter_contracts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_parameter_contracts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parameter_id` int(11) NOT NULL,
  `contract_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `parameter_id` (`parameter_id`,`contract_id`),
  KEY `contract_id_refs_id_2c9b0b73` (`contract_id`),
  CONSTRAINT `contract_id_refs_id_2c9b0b73` FOREIGN KEY (`contract_id`) REFERENCES `pl_contract` (`id`),
  CONSTRAINT `parameter_id_refs_id_991dd64b` FOREIGN KEY (`parameter_id`) REFERENCES `manager_parameter` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=356078 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_parameter_third_party_contracts`
--

DROP TABLE IF EXISTS `manager_parameter_third_party_contracts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_parameter_third_party_contracts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parameter_id` int(11) NOT NULL,
  `contract_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `parameter_id` (`parameter_id`,`contract_id`),
  KEY `contract_id_refs_id_b4e9e664` (`contract_id`),
  CONSTRAINT `contract_id_refs_id_b4e9e664` FOREIGN KEY (`contract_id`) REFERENCES `pl_contract` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=661 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_parametercategory`
--

DROP TABLE IF EXISTS `manager_parametercategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_parametercategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) NOT NULL,
  `category_type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_settings`
--

DROP TABLE IF EXISTS `manager_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_settings` (
  `user_id` int(11) NOT NULL,
  `settings` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `user_id_refs_id_23e6f657` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_shareablesetting`
--

DROP TABLE IF EXISTS `manager_shareablesetting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_shareablesetting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_id` int(11) NOT NULL,
  `account_id` varchar(128) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `category` varchar(45) NOT NULL,
  `name` varchar(50) NOT NULL,
  `timestamp` datetime NOT NULL,
  `filter_json` longtext NOT NULL,
  `action_json` longtext NOT NULL,
  `name_filter` longtext,
  PRIMARY KEY (`id`),
  KEY `user_id_refs_id_efffe051` (`user_id`),
  KEY `contract_id` (`contract_id`),
  CONSTRAINT `manager_shareablesetting_ibfk_1` FOREIGN KEY (`contract_id`) REFERENCES `pl_contract` (`id`),
  CONSTRAINT `user_id_refs_id_efffe051` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=449 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_shareablesetting_groups`
--

DROP TABLE IF EXISTS `manager_shareablesetting_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_shareablesetting_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shareablesetting_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `shareablesetting_id` (`shareablesetting_id`,`group_id`),
  KEY `group_id_refs_id_bf5330ae` (`group_id`),
  CONSTRAINT `group_id_refs_id_bf5330ae` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `shareablesetting_id_refs_id_263f827b` FOREIGN KEY (`shareablesetting_id`) REFERENCES `manager_shareablesetting` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_view`
--

DROP TABLE IF EXISTS `manager_view`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_view` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `context` varchar(2) NOT NULL,
  `label` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=272 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_view_pl_contract`
--

DROP TABLE IF EXISTS `manager_view_pl_contract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_view_pl_contract` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `view_id` int(11) NOT NULL,
  `contract_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `view_id` (`view_id`,`contract_id`),
  KEY `contract_id_refs_id_a232ecc6` (`contract_id`),
  CONSTRAINT `contract_id_refs_id_a232ecc6` FOREIGN KEY (`contract_id`) REFERENCES `pl_contract` (`id`),
  CONSTRAINT `view_id_refs_id_bf597a35` FOREIGN KEY (`view_id`) REFERENCES `manager_view` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1734 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager_viewparamrelationship`
--

DROP TABLE IF EXISTS `manager_viewparamrelationship`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_viewparamrelationship` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `view_id` int(11) NOT NULL,
  `parameter_id` int(11) NOT NULL,
  `is_agg` tinyint(1) NOT NULL,
  `agg_type` varchar(2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `view_id` (`view_id`,`parameter_id`),
  KEY `parameter_id_refs_id_2e928986` (`parameter_id`),
  CONSTRAINT `parameter_id_refs_id_2e928986` FOREIGN KEY (`parameter_id`) REFERENCES `manager_parameter` (`id`),
  CONSTRAINT `view_id_refs_id_fa1c3541` FOREIGN KEY (`view_id`) REFERENCES `manager_view` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1535 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pagepost_creator_workflow_data`
--

DROP TABLE IF EXISTS `pagepost_creator_workflow_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pagepost_creator_workflow_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(100) DEFAULT NULL,
  `content` longtext NOT NULL,
  `post_type` varchar(100) DEFAULT NULL,
  `date_uploaded` datetime NOT NULL,
  `contract_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `page_name_id` varchar(128) DEFAULT NULL,
  `job_status` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_refs_id_eeec355f` (`user_id`),
  KEY `account_id_refs_id_49173b5c` (`account_id`),
  KEY `contract_id_refs_id_67b1a2e2` (`contract_id`),
  CONSTRAINT `account_id_refs_id_49173b5c` FOREIGN KEY (`account_id`) REFERENCES `facebook_account` (`id`),
  CONSTRAINT `contract_id_refs_id_67b1a2e2` FOREIGN KEY (`contract_id`) REFERENCES `pl_contract` (`id`),
  CONSTRAINT `user_id_refs_id_eeec355f` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pl_acquisitionlevel`
--

DROP TABLE IF EXISTS `pl_acquisitionlevel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pl_acquisitionlevel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `calfinder_mapping` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pl_acquisitiontype`
--

DROP TABLE IF EXISTS `pl_acquisitiontype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pl_acquisitiontype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `long_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `source` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pl_acquisitiontype_48ee9dea` (`source`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pl_agency`
--

DROP TABLE IF EXISTS `pl_agency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pl_agency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pl_agency_52094d6e` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pl_client`
--

DROP TABLE IF EXISTS `pl_client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pl_client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pl_client_52094d6e` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=331 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pl_client_accounts`
--

DROP TABLE IF EXISTS `pl_client_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pl_client_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `client_id` (`client_id`,`account_id`),
  KEY `account_id_refs_id_bed6c730` (`account_id`),
  CONSTRAINT `account_id_refs_id_bed6c730` FOREIGN KEY (`account_id`) REFERENCES `facebook_account` (`id`),
  CONSTRAINT `client_id_refs_id_28e6b93f` FOREIGN KEY (`client_id`) REFERENCES `pl_client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2835 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pl_client_agencies`
--

DROP TABLE IF EXISTS `pl_client_agencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pl_client_agencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `agency_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `client_id` (`client_id`,`agency_id`),
  KEY `agency_id_refs_id_dc5a6058` (`agency_id`),
  CONSTRAINT `agency_id_refs_id_dc5a6058` FOREIGN KEY (`agency_id`) REFERENCES `pl_agency` (`id`),
  CONSTRAINT `client_id_refs_id_abfc08cc` FOREIGN KEY (`client_id`) REFERENCES `pl_client` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=271 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pl_client_pages`
--

DROP TABLE IF EXISTS `pl_client_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pl_client_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `client_id` (`client_id`,`page_id`),
  KEY `page_id_refs_id_12519b66` (`page_id`),
  CONSTRAINT `client_id_refs_id_98238a7a` FOREIGN KEY (`client_id`) REFERENCES `pl_client` (`id`),
  CONSTRAINT `page_id_refs_id_12519b66` FOREIGN KEY (`page_id`) REFERENCES `facebook_page` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=373 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pl_client_users`
--

DROP TABLE IF EXISTS `pl_client_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pl_client_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `client_id` (`client_id`,`user_id`),
  KEY `user_id_refs_id_e849bab8` (`user_id`),
  CONSTRAINT `client_id_refs_id_7d645b8e` FOREIGN KEY (`client_id`) REFERENCES `pl_client` (`id`),
  CONSTRAINT `user_id_refs_id_e849bab8` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3860 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pl_contract`
--

DROP TABLE IF EXISTS `pl_contract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pl_contract` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `contracttype_id` int(11) NOT NULL,
  `fee` decimal(10,4) NOT NULL,
  `acquisitiontype_id` int(11) NOT NULL,
  `secondary_acquisitiontype_id` int(11) DEFAULT NULL,
  `calculator` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `css` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `conversion_query` longtext COLLATE utf8_unicode_ci,
  `conversion2_query` longtext COLLATE utf8_unicode_ci,
  `conversion3_query` longtext COLLATE utf8_unicode_ci,
  `sdate` datetime NOT NULL,
  `edate` datetime NOT NULL,
  `iototal` decimal(20,4) NOT NULL,
  `cpmtargetmarginperc` decimal(10,4) DEFAULT NULL,
  `logo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rfp` tinyint(1) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0' COMMENT 'parent contract; this is record is a sub-contract or a portion there of',
  `timezone_id` int(11) DEFAULT NULL,
  `ref_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `acquisitionlevel_id` int(11) DEFAULT NULL,
  `downstream_id` int(11) DEFAULT NULL,
  `revenuetype_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pl_contract_4a4e8ffb` (`client_id`),
  KEY `pl_contract_d9bb20d6` (`contracttype_id`),
  KEY `pl_contract_60dc8842` (`acquisitiontype_id`),
  KEY `pl_contract_411bd5e9` (`sdate`),
  KEY `pl_contract_a8074ec7` (`edate`),
  KEY `secondary_acquisitiontype_id_refs_id_2648eb2b` (`secondary_acquisitiontype_id`),
  KEY `timezone_id_refs_id_5a3877d0` (`timezone_id`),
  KEY `currency_id_refs_id_6e7cd284` (`currency_id`),
  KEY `acquisitionlevel_id_refs_id_e55d0c16` (`acquisitionlevel_id`),
  KEY `downstream_id_refs_id_e55d0c16` (`downstream_id`),
  KEY `revenuetype_id_refs_id_2648eb2b` (`revenuetype_id`),
  CONSTRAINT `acquisitionlevel_id_refs_id_e55d0c16` FOREIGN KEY (`acquisitionlevel_id`) REFERENCES `pl_acquisitionlevel` (`id`),
  CONSTRAINT `acquisitiontype_id_refs_id_2648eb2b` FOREIGN KEY (`acquisitiontype_id`) REFERENCES `pl_acquisitiontype` (`id`),
  CONSTRAINT `client_id_refs_id_c7b4b3f4` FOREIGN KEY (`client_id`) REFERENCES `pl_client` (`id`),
  CONSTRAINT `contracttype_id_refs_id_3e4c882b` FOREIGN KEY (`contracttype_id`) REFERENCES `pl_contracttype` (`id`),
  CONSTRAINT `currency_id_refs_id_6e7cd284` FOREIGN KEY (`currency_id`) REFERENCES `facebook_currency` (`id`),
  CONSTRAINT `downstream_id_refs_id_e55d0c16` FOREIGN KEY (`downstream_id`) REFERENCES `pl_acquisitionlevel` (`id`),
  CONSTRAINT `revenuetype_id_refs_id_2648eb2b` FOREIGN KEY (`revenuetype_id`) REFERENCES `pl_acquisitiontype` (`id`),
  CONSTRAINT `secondary_acquisitiontype_id_refs_id_2648eb2b` FOREIGN KEY (`secondary_acquisitiontype_id`) REFERENCES `pl_acquisitiontype` (`id`),
  CONSTRAINT `timezone_id_refs_id_5a3877d0` FOREIGN KEY (`timezone_id`) REFERENCES `facebook_timezone` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1691 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pl_contractaccountmap`
--

DROP TABLE IF EXISTS `pl_contractaccountmap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pl_contractaccountmap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `contract_id` int(11) DEFAULT NULL,
  `sdate` datetime NOT NULL,
  `edate` datetime NOT NULL,
  `overwritten_active` tinyint(1) DEFAULT '0',
  `overwritten_inactive` tinyint(1) DEFAULT '0',
  `synch_deleted_adgroups` tinyint(4) NOT NULL DEFAULT '1',
  `priority` smallint(6) NOT NULL DEFAULT '1' COMMENT 'FOR SAUL ONLY/SAMSUNG TOOLS',
  PRIMARY KEY (`id`),
  KEY `pl_contractaccountmap_6f2fe10e` (`account_id`),
  KEY `pl_contractaccountmap_b07bb6ae` (`contract_id`),
  KEY `pl_contractaccountmap_411bd5e9` (`sdate`),
  KEY `pl_contractaccountmap_a8074ec7` (`edate`),
  CONSTRAINT `account_id_refs_id_f1bdab5e` FOREIGN KEY (`account_id`) REFERENCES `facebook_account` (`id`),
  CONSTRAINT `contract_id_refs_id_8be67664` FOREIGN KEY (`contract_id`) REFERENCES `pl_contract` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3185 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pl_contracttype`
--

DROP TABLE IF EXISTS `pl_contracttype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pl_contracttype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `templates_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pl_contracttype_5df4c82a` (`alias`),
  KEY `templates_id_refs_id_299ad629` (`templates_id`),
  CONSTRAINT `templates_id_refs_id_299ad629` FOREIGN KEY (`templates_id`) REFERENCES `pl_templates` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pl_contractusertemplatemap`
--

DROP TABLE IF EXISTS `pl_contractusertemplatemap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pl_contractusertemplatemap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `templates_id` int(11) NOT NULL,
  `sdate` datetime NOT NULL,
  `edate` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pl_contractusertemplatemap_b07bb6ae` (`contract_id`),
  KEY `pl_contractusertemplatemap_fbfc09f1` (`user_id`),
  KEY `pl_contractusertemplatemap_dc6831f8` (`templates_id`),
  CONSTRAINT `contract_id_refs_id_89a8f153` FOREIGN KEY (`contract_id`) REFERENCES `pl_contract` (`id`),
  CONSTRAINT `templates_id_refs_id_b7b92611` FOREIGN KEY (`templates_id`) REFERENCES `pl_templates` (`id`),
  CONSTRAINT `user_id_refs_id_9f500be` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8387 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pl_parentcontract`
--

DROP TABLE IF EXISTS `pl_parentcontract`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pl_parentcontract` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `logo_url` varchar(100) NOT NULL DEFAULT '',
  `logo_height` varchar(100) NOT NULL DEFAULT '',
  `logo_width` varchar(100) NOT NULL DEFAULT '',
  `logo_alt` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=310 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pl_templates`
--

DROP TABLE IF EXISTS `pl_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pl_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `calculator` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pl_templates_6a2c14f2` (`template_name`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sandbox_cmsettings`
--

DROP TABLE IF EXISTS `sandbox_cmsettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sandbox_cmsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `filter` varchar(5000) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `user_id_refs_id_6a42561d` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sandbox_fakeform`
--

DROP TABLE IF EXISTS `sandbox_fakeform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sandbox_fakeform` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) NOT NULL,
  `age` int(11) NOT NULL,
  `zipcode` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sandbox_trigger`
--

DROP TABLE IF EXISTS `sandbox_trigger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sandbox_trigger` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_id` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `metric_name` varchar(20) NOT NULL,
  `value` int(11) NOT NULL,
  `time_interval` varchar(20) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `pause` tinyint(1) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `spend_cap` int(11) DEFAULT '-1',
  PRIMARY KEY (`id`),
  KEY `campaign_id_refs_id_b795c92c` (`campaign_id`),
  KEY `account_id_refs_id_74fd1450` (`account_id`),
  KEY `contract_id_refs_id_d7a3512a` (`contract_id`),
  KEY `user_id_refs_id_6e9a396b` (`user_id`),
  CONSTRAINT `account_id_refs_id_74fd1450` FOREIGN KEY (`account_id`) REFERENCES `facebook_account` (`id`),
  CONSTRAINT `campaign_id_refs_id_b795c92c` FOREIGN KEY (`campaign_id`) REFERENCES `facebook_campaign` (`id`),
  CONSTRAINT `contract_id_refs_id_d7a3512a` FOREIGN KEY (`contract_id`) REFERENCES `pl_contract` (`id`),
  CONSTRAINT `user_id_refs_id_6e9a396b` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `schactions_action`
--

DROP TABLE IF EXISTS `schactions_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schactions_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contract_id` int(11) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `account_id` varchar(128) DEFAULT NULL,
  `action_filter` varchar(2000) NOT NULL,
  `metrics_range` int(11) NOT NULL,
  `metrics_range_shift` int(11) DEFAULT '0',
  `name_filter` varchar(2000) NOT NULL,
  `filter_json` varchar(2000) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `frequency` int(11) NOT NULL,
  `custom_frequency` varchar(2000) DEFAULT NULL,
  `last_executed` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `action_min` double DEFAULT NULL,
  `action_max` double DEFAULT NULL,
  `visible` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `user_id_refs_id_361b8a8b` (`user_id`),
  KEY `schactions_action_b07bb6ae` (`contract_id`),
  CONSTRAINT `contract_id_refs_id_716f6b2c` FOREIGN KEY (`contract_id`) REFERENCES `pl_contract` (`id`),
  CONSTRAINT `user_id_refs_id_361b8a8b` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21157 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sherlock_goals`
--

DROP TABLE IF EXISTS `sherlock_goals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sherlock_goals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goal_type` varchar(20) NOT NULL,
  `goal` double NOT NULL,
  `upd_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `targeting_audiencegroup`
--

DROP TABLE IF EXISTS `targeting_audiencegroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `targeting_audiencegroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `contract_id` int(11) NOT NULL,
  `sdate` date NOT NULL,
  `edate` date NOT NULL,
  `budget` decimal(10,2) NOT NULL,
  `acquisitions` decimal(10,2) NOT NULL,
  `cpa` decimal(10,2) NOT NULL,
  `roas` decimal(10,2) NOT NULL,
  `reach` decimal(10,2) NOT NULL,
  `frequency` decimal(10,2) NOT NULL,
  `budget_scope` varchar(1) NOT NULL,
  `acquisition_scope` varchar(1) NOT NULL,
  `query` longtext,
  PRIMARY KEY (`id`),
  KEY `contract_id_refs_id_a5e2b6d3` (`contract_id`),
  CONSTRAINT `contract_id_refs_id_a5e2b6d3` FOREIGN KEY (`contract_id`) REFERENCES `pl_contract` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=173 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `targeting_audiencegroup_dimensions`
--

DROP TABLE IF EXISTS `targeting_audiencegroup_dimensions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `targeting_audiencegroup_dimensions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `audiencegroup_id` int(11) NOT NULL,
  `dimensionvalue_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `audiencegroup_id` (`audiencegroup_id`,`dimensionvalue_id`),
  KEY `dimensionvalue_id_refs_id_dbf6e50f` (`dimensionvalue_id`),
  CONSTRAINT `audiencegroup_id_refs_id_cd9a2e44` FOREIGN KEY (`audiencegroup_id`) REFERENCES `targeting_audiencegroup` (`id`),
  CONSTRAINT `dimensionvalue_id_refs_id_dbf6e50f` FOREIGN KEY (`dimensionvalue_id`) REFERENCES `targeting_dimensionvalue` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `targeting_dimension`
--

DROP TABLE IF EXISTS `targeting_dimension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `targeting_dimension` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `contract_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`,`contract_id`),
  KEY `contract_id_refs_id_2ba04596` (`contract_id`),
  CONSTRAINT `contract_id_refs_id_2ba04596` FOREIGN KEY (`contract_id`) REFERENCES `pl_contract` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `targeting_dimensionvalue`
--

DROP TABLE IF EXISTS `targeting_dimensionvalue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `targeting_dimensionvalue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `query` longtext,
  `parent_dimension_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`,`parent_dimension_id`),
  KEY `parent_dimension_id_refs_id_2e0d4abb` (`parent_dimension_id`),
  CONSTRAINT `parent_dimension_id_refs_id_2e0d4abb` FOREIGN KEY (`parent_dimension_id`) REFERENCES `targeting_dimension` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `temp1`
--

DROP TABLE IF EXISTS `temp1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temp1` (
  `adgroup_tracker_id` int(11) NOT NULL,
  `action_type` varchar(100) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime DEFAULT NULL,
  `count(*)` bigint(21) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `temp_adtt`
--

DROP TABLE IF EXISTS `temp_adtt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temp_adtt` (
  `id` int(11) NOT NULL DEFAULT '0',
  `timestamp` datetime NOT NULL,
  `sdatetime` datetime NOT NULL,
  `edatetime` datetime NOT NULL,
  `sdatetime_fb` datetime DEFAULT NULL,
  `edatetime_fb` datetime DEFAULT NULL,
  `impressions` int(11) NOT NULL,
  `clicks` int(11) NOT NULL,
  `spent` double NOT NULL,
  `connections` int(11) NOT NULL,
  `social_impressions` int(11) NOT NULL,
  `social_clicks` int(11) NOT NULL,
  `social_unique_impressions` int(11) NOT NULL,
  `social_unique_clicks` int(11) NOT NULL,
  `unique_impressions` int(11) NOT NULL,
  `unique_clicks` int(11) NOT NULL,
  `targeted` int(11) DEFAULT NULL,
  `actions` int(11) DEFAULT NULL,
  `sdatetime_as` datetime DEFAULT NULL,
  `edatetime_as` datetime DEFAULT NULL,
  `conversions` int(11) NOT NULL,
  `acquisitions` int(11) NOT NULL,
  `revenue` double NOT NULL,
  `account_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tracker_currency`
--

DROP TABLE IF EXISTS `tracker_currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tracker_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `currency` varchar(3) NOT NULL,
  `sdate` datetime NOT NULL,
  `edate` datetime NOT NULL,
  `rate` double NOT NULL,
  `alias` varchar(30) NOT NULL,
  `symbol` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `unemployer_expandedset`
--

DROP TABLE IF EXISTS `unemployer_expandedset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unemployer_expandedset` (
  `phraseset_ptr_id` int(11) NOT NULL,
  `stemphraseid` int(11) NOT NULL,
  PRIMARY KEY (`phraseset_ptr_id`),
  KEY `unemployer_expandedset_96527228` (`stemphraseid`),
  CONSTRAINT `phraseset_ptr_id_refs_id_fd6fd1b9` FOREIGN KEY (`phraseset_ptr_id`) REFERENCES `unemployer_phraseset` (`id`),
  CONSTRAINT `stemphraseid_refs_id_514ceb32` FOREIGN KEY (`stemphraseid`) REFERENCES `unemployer_phrase` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `unemployer_job`
--

DROP TABLE IF EXISTS `unemployer_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unemployer_job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sdate` datetime NOT NULL,
  `edate` datetime NOT NULL,
  `stemsetid` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `queued` tinyint(1) NOT NULL,
  `processed` tinyint(1) NOT NULL,
  `notified` tinyint(1) NOT NULL,
  `targetingspec_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `unemployer_job_a8074ec7` (`edate`),
  KEY `unemployer_job_4d67f478` (`stemsetid`),
  CONSTRAINT `stemsetid_refs_phraseset_ptr_id_efd3b8e1` FOREIGN KEY (`stemsetid`) REFERENCES `unemployer_stemset` (`phraseset_ptr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=818 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `unemployer_phrase`
--

DROP TABLE IF EXISTS `unemployer_phrase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unemployer_phrase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sdate` datetime NOT NULL,
  `edate` datetime NOT NULL,
  `phrase` varchar(250) CHARACTER SET latin1 NOT NULL,
  `targetsize` int(11) DEFAULT NULL,
  `targetsizedate` datetime DEFAULT NULL,
  `expandedsetid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `unemployer_phrase_6b89aede` (`phrase`),
  KEY `unemployer_phrase_a5aa38d4` (`expandedsetid`),
  CONSTRAINT `expandedsetid_refs_phraseset_ptr_id_5a575bd0` FOREIGN KEY (`expandedsetid`) REFERENCES `unemployer_expandedset` (`phraseset_ptr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1461994 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `unemployer_phrasequeue`
--

DROP TABLE IF EXISTS `unemployer_phrasequeue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unemployer_phrasequeue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sdate` datetime NOT NULL,
  `edate` datetime NOT NULL,
  `qdate` datetime DEFAULT NULL,
  `psdate` datetime DEFAULT NULL,
  `pedate` datetime DEFAULT NULL,
  `stemphraseid` int(11) NOT NULL,
  `jobid` int(11) DEFAULT NULL,
  `expandedsetid` int(11) DEFAULT NULL,
  `processed` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `unemployer_phrasequeue_22a8610a` (`psdate`),
  KEY `unemployer_phrasequeue_96527228` (`stemphraseid`),
  KEY `unemployer_phrasequeue_d410fa00` (`jobid`),
  KEY `unemployer_phrasequeue_a5aa38d4` (`expandedsetid`),
  CONSTRAINT `expandedsetid_refs_phraseset_ptr_id_a5b5ff8e` FOREIGN KEY (`expandedsetid`) REFERENCES `unemployer_expandedset` (`phraseset_ptr_id`),
  CONSTRAINT `jobid_refs_id_c877a6ba` FOREIGN KEY (`jobid`) REFERENCES `unemployer_job` (`id`),
  CONSTRAINT `stemphraseid_refs_id_4cfa91e7` FOREIGN KEY (`stemphraseid`) REFERENCES `unemployer_phrase` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10954 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `unemployer_phraseset`
--

DROP TABLE IF EXISTS `unemployer_phraseset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unemployer_phraseset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sdate` datetime NOT NULL,
  `edate` datetime NOT NULL,
  `phrases` varchar(3000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21285 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `unemployer_status`
--

DROP TABLE IF EXISTS `unemployer_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unemployer_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sdate` datetime NOT NULL,
  `edate` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70947 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `unemployer_stemset`
--

DROP TABLE IF EXISTS `unemployer_stemset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unemployer_stemset` (
  `phraseset_ptr_id` int(11) NOT NULL,
  PRIMARY KEY (`phraseset_ptr_id`),
  CONSTRAINT `phraseset_ptr_id_refs_id_8c2a7bcb` FOREIGN KEY (`phraseset_ptr_id`) REFERENCES `unemployer_phraseset` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `version`
--

DROP TABLE IF EXISTS `version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `version` (
  `name` varchar(128) NOT NULL,
  `version` varchar(64) NOT NULL,
  `last_modified` datetime NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`name`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-17  0:15:24
