INSERT INTO `auth_user` (`is_active`, `email`, `date_joined`, `username`, `password`, `first_name`, `last_name`, `last_login`, `is_superuser`, `id`, `is_staff`) VALUES (1, "v", "2013-05-29 16:02:33", "v", "v", "v", "v", "2013-05-29 16:02:33", 1, 1, 1);

INSERT INTO `pl_acquisitiontype` (`id`, `type_name`, `source`) VALUES (1, "v", "v");

INSERT INTO `pl_acquisitionlevel` (`id`, `calfinder_mapping`, `name`) VALUES (1, "v", "v");

INSERT INTO `facebook_currency` (`name`, `code`, `id`, `offset`) VALUES ("v", "v", 1, 1);

INSERT INTO `facebook_timezone` (`id`, `timezone_id`, `name`) VALUES (1, 1, "v");

INSERT INTO `pl_templates` (`id`, `template_name`) VALUES (1, "v");

INSERT INTO `pl_contracttype` (`id`, `type_name`) VALUES (1, "v");

INSERT INTO `pl_client` (`id`, `name`) VALUES (1, "v");

INSERT INTO `pl_contract` (`acquisitiontype_id`, `sdate`, `fee`, `contracttype_id`, `iototal`, `client_id`, `id`, `edate`) VALUES (1, "2013-05-29 16:02:33", 0.21345, 1, 0.21345, 1, 1, "2013-05-29 16:02:33");

INSERT INTO `manager_shareablesetting` (`name`, `contract_id`, `timestamp`, `action_json`, `category`, `filter_json`, `id`) VALUES ("v", 1, "2013-05-29 16:02:33", "longtext", "v", "longtext", 1);

INSERT INTO `account_credential` (`id`, `service`, `password`, `login`) VALUES (1, "v", "v", "v");

INSERT INTO `facebook_accountstatus` (`id`, `name`) VALUES (1, "v");

INSERT INTO `facebook_account` (`permitted_roles`, `updated_on`, `handshake_completed`, `bm_linked`, `access_type`, `created_on`, `fbid`, `id`, `name`, `synch_limit`, `retired_on`, `timezone`) VALUES ("v", "2013-05-29 16:02:33", 1, 1, "v", "2013-05-29 16:02:33", "v", 1, "v", 1, "2013-05-29 16:02:33", "v");

INSERT INTO `audiences_audience` (`account_id`, `subtype`, `created_on`, `name`, `updated_on`, `description`, `subtype_name`, `fbid`, `type_name`, `sent_count`, `opt_out_url`, `type`, `rule`, `parent_category`, `id`, `status`, `approximate_count`) VALUES (1, 1, "2013-05-29 16:02:33", "v", "2013-05-29 16:02:33", "v", "v", 1, "v", 1, "v", 1, "v", "v", 1, 1, 1);

INSERT INTO `unemployer_phraseset` (`id`, `sdate`, `phrases`, `edate`) VALUES (1, "2013-05-29 16:02:33", "v", "2013-05-29 16:02:33");

INSERT INTO `unemployer_phrase` (`edate`, `sdate`, `phrase`, `id`) VALUES ("2013-05-29 16:02:33", "2013-05-29 16:02:33", "v", 1);

INSERT INTO `unemployer_expandedset` (`phraseset_ptr_id`, `stemphraseid`) VALUES (1, 1);

INSERT INTO `facebook_adheadline` (`id`, `headline`) VALUES (1, "v");

INSERT INTO `facebook_adcreative` (`updated`, `id`) VALUES (1, 1);

INSERT INTO `facebook_adbody` (`id`, `body`) VALUES (1, "v");

INSERT INTO `facebook_adspec` (`url`, `link_url`, `type`, `objectType`, `id`) VALUES ("v", "v", 1, 1, 1);

INSERT INTO `manager_view` (`id`, `context`, `name`, `label`) VALUES (1, "v", "v", "v");

INSERT INTO `facebook_creativespec` (`created_on`, `fbid`, `updated_on`, `retired_on`, `id`) VALUES ("2013-05-29 16:02:33", "v", "2013-05-29 16:02:33", "2013-05-29 16:02:33", 1);

INSERT INTO `facebook_campaign` (`created_on`, `name`, `max_bid`, `lifetime_budget`, `fbid`, `start_time`, `bid_type`, `daily_budget`, `updated_on`, `retired_on`, `id`, `status`) VALUES ("2013-05-29 16:02:33", "v", 0.21345, 0.21345, "v", "2013-05-29 16:02:33", 1, 0.21345, "2013-05-29 16:02:33", "2013-05-29 16:02:33", 1, 1);

INSERT INTO `facebook_account_campaigns` (`id`, `campaign_id`, `account_id`) VALUES (1, 1, 1);

INSERT INTO `account_profile` (`user_id`, `bm_linked`, `fbid`, `bm_role`, `fb_email`, `id`) VALUES (1, 1, "v", "v", "v", 1);

INSERT INTO `account_apicredentials` (`id`, `service`, `api_key`, `profile_id`) VALUES (1, "v", "v", 1);

INSERT INTO `djcelery_crontabschedule` (`minute`, `hour`, `day_of_week`, `month_of_year`, `day_of_month`, `id`) VALUES ("v", "v", "v", "v", "v", 1);

INSERT INTO `facebook_ad` (`id`, `creative_spec`, `adSpecid`) VALUES (1, 1, 1);

INSERT INTO `facebook_adgroup` (`created_on`, `name`, `max_bid`, `fbid`, `bid_type`, `updated_on`, `retired_on`, `id`, `status`) VALUES ("2013-05-29 16:02:33", "v", 0.21345, "v", 1, "2013-05-29 16:02:33", "2013-05-29 16:02:33", 1, 1);

INSERT INTO `ad_creator_workflow` (`id`, `name`) VALUES (1, "longtext");

INSERT INTO `ad_creator_workflow_adgroups` (`adgroup_id`, `id`, `workflow_id`) VALUES (1, 1, 1);

INSERT INTO `facebook_etag` (`id`, `timestamp`, `url`, `etag`) VALUES (1, "2013-05-29 16:02:33", "v", "v");

INSERT INTO `unemployer_stemset` (`phraseset_ptr_id`) VALUES (1);

INSERT INTO `account_profile_credentials` (`credential_id`, `id`, `profile_id`) VALUES (1, 1, 1);

INSERT INTO `manager_parametercategory` (`id`, `category_type`, `category_name`) VALUES (1, "v", "v");

INSERT INTO `sandbox_trigger` (`value`, `metric_name`, `time_interval`, `created_at`, `active`, `pause`, `updated_at`, `id`) VALUES (1, "v", "v", "2013-05-29 16:02:33", 1, 1, "2013-05-29 16:02:33", 1);

INSERT INTO `targeting_audiencegroup` (`roas`, `acquisition_scope`, `name`, `contract_id`, `budget`, `acquisitions`, `description`, `edate`, `budget_scope`, `sdate`, `id`, `cpa`, `reach`, `frequency`) VALUES (0.21345, "v", "v", 1, 0.21345, 0.21345, "longtext", "2013-05-29", "v", "2013-05-29", 1, 0.21345, 0.21345, 0.21345);

INSERT INTO `conversion_source` (`id`) VALUES (1);

INSERT INTO `celery_tasksetmeta` (`id`, `hidden`, `taskset_id`, `date_done`, `result`) VALUES (1, 1, "v", "2013-05-29 16:02:33", "longtext");

INSERT INTO `facebook_account_users` (`user_id`, `id`, `account_id`) VALUES (1, 1, 1);

INSERT INTO `sandbox_fakeform` (`id`, `zipcode`, `age`, `email`) VALUES (1, "v", 1, "v");

INSERT INTO `facebook_reportspec` (`aggregation_time`, `name`, `start_time`, `summarize_by`, `id`, `report_type`, `stop_time`) VALUES ("v", "v", "2013-05-29 16:02:33", "v", 1, "v", "2013-05-29 16:02:33");

INSERT INTO `facebook_schedulespec` (`frequency`, `report_spec_id`, `time_next`, `id`, `status`, `datetime`, `name`) VALUES (1, 1, "2013-05-29 16:02:33", 1, 1, "2013-05-29 16:02:33", "v");

INSERT INTO `ad_creator_locales` (`id`, `key`, `name`) VALUES (1, 1, "longtext");

INSERT INTO `manager_view_pl_contract` (`id`, `view_id`, `contract_id`) VALUES (1, 1, 1);

INSERT INTO `facebook_request` (`queued`, `priority`, `url`, `http_method`, `visible`, `attempts`, `api_method`, `max_attempts`, `id`, `status`, `params`) VALUES ("2013-05-29 16:02:33", 1, "longtext", "v", 1, 1, "v", 1, 1, "v", "longtext");

INSERT INTO `manager_parameter` (`meta`, `name`, `source`, `order`, `id_field`, `id`, `id_value`) VALUES ("longtext", "v", "v", 1, "v", 1, 1);

INSERT INTO `manager_parameter_contracts` (`id`, `parameter_id`, `contract_id`) VALUES (1, 1, 1);

INSERT INTO `auth_group` (`id`, `name`) VALUES (1, "v");

INSERT INTO `account_acl` (`subject`, `items`, `edate`, `sdate`, `category`, `createP`, `deleteP`, `id`, `updateP`, `readP`) VALUES ("v", "longtext", "2013-05-29 16:02:33", "2013-05-29 16:02:33", "v", 1, 1, 1, 1, 1);

INSERT INTO `manager_shareablesetting_groups` (`id`, `group_id`, `shareablesetting_id`) VALUES (1, 1, 1);

INSERT INTO `facebook_locale` (`id`, `locale`) VALUES (1, "v");

INSERT INTO `ad_creator_bulk_job` (`user_id`, `celery_job_id`, `name`, `job_type`, `start_time`, `next_action`, `id`, `status`) VALUES (1, 1, "longtext", "v", "2013-05-29 16:02:33", "v", 1, "v");

INSERT INTO `pl_parentcontract` (`name`, `logo_width`, `logo_alt`, `logo_height`, `logo_url`, `id`) VALUES ("v", "v", "v", "v", "v", 1);

INSERT INTO `temp1` (`adgroup_tracker_id`, `action_type`, `start_time`, `count(*)`) VALUES (1, "v", "2013-05-29 16:02:33", 1);

INSERT INTO `facebook_campaigngroup` (`created_on`, `name`, `budget`, `fbid`, `updated_on`, `retired_on`, `objective`, `id`, `status`) VALUES ("2013-05-29 16:02:33", "v", 0.21345, "v", "2013-05-29 16:02:33", "2013-05-29 16:02:33", "v", 1, 1);

INSERT INTO `facebook_account_campaigngroups` (`id`, `campaigngroup_id`, `account_id`) VALUES (1, 1, 1);

INSERT INTO `facebook_accountstats` (`pending_review`, `campaigns`, `deleted`, `account`, `paused`, `timestamp`, `active`, `id`, `total`, `disapproved`) VALUES (1, 1, 1, "v", 1, "2013-05-29 16:02:33", 1, 1, 1, 1);

INSERT INTO `facebook_campaign_adgroups` (`adgroup_id`, `id`, `campaign_id`) VALUES (1, 1, 1);

INSERT INTO `targeting_dimension` (`id`, `name`, `contract_id`) VALUES (1, "v", 1);

INSERT INTO `targeting_dimensionvalue` (`id`, `parent_dimension_id`, `name`) VALUES (1, 1, "v");

INSERT INTO `targeting_audiencegroup_dimensions` (`dimensionvalue_id`, `id`, `audiencegroup_id`) VALUES (1, 1, 1);

INSERT INTO `pl_agency` (`id`, `name`) VALUES (1, "v");

INSERT INTO `pl_client_agencies` (`id`, `agency_id`, `client_id`) VALUES (1, 1, 1);

INSERT INTO `facebook_pixel` (`account_id`, `tracking_spec`, `name`, `currency`, `fbid`, `tag`, `id`, `status`) VALUES (1, "v", "v", "v", "v", "v", 1, "v");

INSERT INTO `facebook_pixel_accounts` (`id`, `pixel_id`, `account_id`) VALUES (1, 1, 1);

INSERT INTO `djcelery_intervalschedule` (`id`, `every`, `period`) VALUES (1, 1, "v");

INSERT INTO `djcelery_periodictask` (`name`, `enabled`, `args`, `total_run_count`, `date_changed`, `task`, `description`, `id`, `kwargs`) VALUES ("v", 1, "longtext", 1, "2013-05-29 16:02:33", "v", "longtext", 1, "longtext");

INSERT INTO `manager_viewparamrelationship` (`agg_type`, `id`, `is_agg`, `parameter_id`, `view_id`) VALUES ("v", 1, 1, 1, 1);

INSERT INTO `auth_message` (`user_id`, `id`, `message`) VALUES (1, 1, "longtext");

INSERT INTO `conversion_privatekey` (`id`, `contact_specific_ref`, `hash_func`, `pkey`) VALUES (1, "v", "v", "v");

INSERT INTO `conversion_conversiondebugger` (`account_id`, `contract_id`, `edatetime`, `timestamp`, `id`, `sdatetime`) VALUES (1, 1, "2013-05-29 16:02:33", "2013-05-29 16:02:33", 1, "2013-05-29 16:02:33");

INSERT INTO `djcelery_workerstate` (`id`, `hostname`) VALUES (1, "v");

INSERT INTO `facebook_page` (`access_token`, `permitted_roles`, `created_on`, `bm_linked`, `fbid`, `access_type`, `category`, `updated_on`, `id`) VALUES ("longtext", "v", "2013-05-29 16:02:33", 1, "v", "v", "v", "2013-05-29 16:02:33", 1);

INSERT INTO `version` (`last_modified`, `name`, `version`) VALUES ("2013-05-29 16:02:33", "v", "v");

INSERT INTO `pl_client_pages` (`page_id`, `id`, `client_id`) VALUES (1, 1, 1);

INSERT INTO `facebook_account_adgroups` (`adgroup_id`, `id`, `account_id`) VALUES (1, 1, 1);

INSERT INTO `unemployer_status` (`id`, `sdate`, `edate`, `status`) VALUES (1, "2013-05-29 16:02:33", "2013-05-29 16:02:33", 1);

INSERT INTO `django_content_type` (`id`, `model`, `name`, `app_label`) VALUES (1, "v", "v", "v");

INSERT INTO `django_admin_log` (`user_id`, `change_message`, `action_flag`, `object_repr`, `action_time`, `id`) VALUES (1, "longtext", 1, "v", "2013-05-29 16:02:33", 1);

INSERT INTO `ad_creator_ad_validation_map` (`id`, `validation_node`, `creative_type`, `order`) VALUES (1, "v", 1, 1);

INSERT INTO `ad_creator_img_bank` (`id`, `url`, `name`, `hash`) VALUES (1, "longtext", "v", "longtext");

INSERT INTO `pl_client_accounts` (`id`, `client_id`, `account_id`) VALUES (1, 1, 1);

INSERT INTO `facebook_reportspec_filter_adgroups` (`adgroup_id`, `id`, `reportspec_id`) VALUES (1, 1, 1);

INSERT INTO `facebook_campaigngroup_campaigns` (`id`, `campaign_id`, `campaigngroup_id`) VALUES (1, 1, 1);

INSERT INTO `facebook_reportspec_filter_campaigns` (`id`, `reportspec_id`, `campaign_id`) VALUES (1, 1, 1);

INSERT INTO `auth_user_groups` (`user_id`, `id`, `group_id`) VALUES (1, 1, 1);

INSERT INTO `facebook_accountcapability` (`id`, `name`) VALUES (1, "v");

INSERT INTO `facebook_account_capabilities` (`accountcapability_id`, `id`, `account_id`) VALUES (1, 1, 1);

INSERT INTO `facebook_targetingcategory` (`name`, `description`, `fbid`, `type`, `path`, `id`, `audience_size`) VALUES ("v", "longtext", "v", "v", "longtext", 1, 1);

INSERT INTO `facebook_accountgroup` (`created_on`, `name`, `fbid`, `updated_on`, `retired_on`, `is_master`, `id`, `status`, `is_full`) VALUES ("2013-05-29 16:02:33", "v", "v", "2013-05-29 16:02:33", "2013-05-29 16:02:33", 1, 1, 1, 1);

INSERT INTO `facebook_accountgroup_accounts` (`id`, `account_id`, `accountgroup_id`) VALUES (1, 1, 1);

INSERT INTO `facebook_requestlocalservice` (`queued`, `priority`, `url`, `http_method`, `visible`, `attempts`, `api_method`, `max_attempts`, `id`, `status`, `params`) VALUES ("2013-05-29 16:02:33", 1, "longtext", "v", 1, 1, "v", 1, 1, "v", "longtext");

INSERT INTO `celery_taskmeta` (`date_done`, `task_id`, `id`, `status`, `hidden`) VALUES ("2013-05-29 16:02:33", "v", 1, "v", 1);

INSERT INTO `ad_creator_states_countries` (`id`, `code`) VALUES (1, "v");

INSERT INTO `temp_adtt` (`acquisitions`, `social_impressions`, `edatetime`, `social_unique_impressions`, `unique_clicks`, `impressions`, `spent`, `timestamp`, `revenue`, `social_unique_clicks`, `id`, `connections`, `account_id`, `conversions`, `social_clicks`, `clicks`, `unique_impressions`, `sdatetime`) VALUES (1, 1, "2013-05-29 16:02:33", 1, 1, 1, 0.21345, "2013-05-29 16:02:33", 0.21345, 1, 1, 1, 1, 1, 1, 1, 1, "2013-05-29 16:02:33");

INSERT INTO `django_session` (`expire_date`, `session_key`, `session_data`) VALUES ("2013-05-29 16:02:33", "v", "longtext");

INSERT INTO `unemployer_job` (`stemsetid`, `notified`, `edate`, `email`, `sdate`, `queued`, `id`, `targetingspec_id`, `processed`) VALUES (1, 1, "2013-05-29 16:02:33", "v", "2013-05-29 16:02:33", 1, 1, 1, 1);

INSERT INTO `pl_contractaccountmap` (`priority`, `edate`, `sdate`, `synch_deleted_adgroups`, `id`) VALUES (1, "2013-05-29 16:02:33", "2013-05-29 16:02:33", 1, 1);

INSERT INTO `pagepost_creator_workflow_data` (`content`, `date_uploaded`, `id`) VALUES ("longtext", "2013-05-29 16:02:33", 1);

INSERT INTO `analytics_pagevisitrecord` (`user_id`, `visit_date`, `url`, `id`, `params`) VALUES (1, "2013-05-29 16:02:33", "v", 1, "v");

INSERT INTO `schactions_action` (`user_id`, `last_executed`, `metrics_range`, `contract_id`, `name_filter`, `action_filter`, `filter_json`, `id`, `status`, `frequency`) VALUES (1, "2013-05-29 16:02:33", 1, 1, "v", "v", "v", 1, 1, 1);

INSERT INTO `djcelery_taskstate` (`state`, `retries`, `task_id`, `tstamp`, `id`, `hidden`) VALUES ("v", 1, "v", "2013-05-29 16:02:33", 1, 1);

INSERT INTO `conversion_conversion` (`session_id`, `ip`, `event`, `adid`, `uid`, `id`, `convtype`, `datetime`) VALUES ("v", "c", "v", "v", "v", 1, "v", "2013-05-29 16:02:33");

INSERT INTO `pl_client_users` (`user_id`, `id`, `client_id`) VALUES (1, 1, 1);

INSERT INTO `conversion_affiliate` (`id`, `aff`) VALUES (1, "v");

INSERT INTO `ad_creator_bulk_job_details` (`content`, `level`, `row_number`, `bulk_job_id`, `id`, `status`, `ad_type_num`) VALUES ("longtext", "c", 1, 1, 1, "v", 1);

INSERT INTO `auth_permission` (`id`, `codename`, `name`, `content_type_id`) VALUES (1, "v", "v", 1);

INSERT INTO `unemployer_phrasequeue` (`edate`, `stemphraseid`, `sdate`, `id`, `processed`) VALUES ("2013-05-29 16:02:33", 1, "2013-05-29 16:02:33", 1, 1);

INSERT INTO `facebook_tokensaudittrail` (`event_type`, `event_ts`, `insert_ts`, `token_type`, `id`) VALUES ("v", 1, "2013-05-29 16:02:33", "v", 1);

INSERT INTO `django_site` (`id`, `domain`, `name`) VALUES (1, "v", "v");

INSERT INTO `pl_contractusertemplatemap` (`user_id`, `edate`, `templates_id`, `sdate`, `id`) VALUES (1, "2013-05-29 16:02:33", 1, "2013-05-29 16:02:33", 1);

INSERT INTO `sandbox_cmsettings` (`user_id`, `id`, `filter`) VALUES (1, 1, "v");

INSERT INTO `auth_user_user_permissions` (`user_id`, `id`, `permission_id`) VALUES (1, 1, 1);

INSERT INTO `conversion_conversioncounts` (`id`, `conv_level`, `conversion`, `hits`) VALUES (1, 1, 1, 1);

INSERT INTO `sherlock_goals` (`id`, `upd_time`, `goal`, `goal_type`) VALUES (1, "2013-05-29 16:02:33", 0.21345, "v");

INSERT INTO `facebook_btcategory` (`parent`, `id`, `fbid`, `name`) VALUES ("v", 1, "v", "v");

INSERT INTO `manager_settings` (`user_id`, `settings`) VALUES (1, "v");

INSERT INTO `auth_group_permissions` (`id`, `permission_id`, `group_id`) VALUES (1, 1, 1);

INSERT INTO `consolidation_result` (`checked`, `subject`, `format`, `timestamp`, `edate`, `failed`, `sdate`, `comparison_engine`, `item`, `discrepancies`, `id`) VALUES (1, "v", "v", "2013-05-29 16:02:33", "2013-05-29 16:02:33", 1, "2013-05-29 16:02:33", "v", "v", "v", 1);

INSERT INTO `tracker_currency` (`currency`, `rate`, `symbol`, `edate`, `sdate`, `id`, `alias`) VALUES ("v", 0.21345, "v", "2013-05-29 16:02:33", "2013-05-29 16:02:33", 1, "v");

INSERT INTO `djcelery_periodictasks` (`last_update`, `ident`) VALUES ("2013-05-29 16:02:33", 1);

INSERT INTO `manager_parameter_third_party_contracts` (`id`, `parameter_id`, `contract_id`) VALUES (1, 1, 1);

