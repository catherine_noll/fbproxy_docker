redis_is_running () {
    ps aux | grep -v grep | grep -q redis
}

until redis_is_running ; do sleep 1 ; done
