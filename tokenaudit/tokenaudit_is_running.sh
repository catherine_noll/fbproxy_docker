tokenaudit_is_running () {
    ps aux | grep -v grep | grep -q tokenaudit
}

until tokenaudit_is_running ; do sleep 1 ; done
