from flask import Flask, jsonify

app = Flask(__name__)


@app.route('/tokenaudit/api/v1.0')
def return_ok():
    return jsonify({"response": "OK"})

if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0', port=7070)
