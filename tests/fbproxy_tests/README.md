This suite automates tests for features outlined as Phase I components of FB Proxy in the 5/27/2015 Facebook Proxy Product Requirements Document (PRD; https://ampush.atlassian.net/wiki/pages/viewpage.action?pageId=66584724).

The test suite is divided into 2 parts: a framework that runs automated python scripts and end-to-end selenium tests.

Automated test scripts bypass the UI to send requests directly to FB Proxy. These should primarily run with FB Proxy routed to API Mocker instead of Facebook.  Instructions for installing API Mocker are below.  

End-to-end tests use selenium to send requests from the UI to FB Proxy via insight, then ssh into the selected AWS environment to run helper scripts that verify that FB Proxy is handling requests as expected. End-to-end tests should be run with FB Proxy routed to Facebook.

<hr>

To set up test environment:

Install API Mocker
```sh
APIMOCKER_TMP_DIR="$(mktemp -d)"
APIMOCKER_BIN_DIR="$APIMOCKER_TMP_DIR/bin"
APIMOCKER_DIR="/home/ampush/services/apimocker"
APIMOCKER_BRANCH="fbproxytest-return-errors"
APIMOCKER_REPO="git@bitbucket.org:ampushsocial/fb-ads-api-mocker.git"

git clone ${APIMOCKER_REPO} ${APIMOCKER_DIR}
cd ${APIMOCKER_DIR}
git fetch
git checkout ${APIMOCKER_BRANCH}
mkdir -p logs
virtualenv $APIMOCKER_TMP_DIR
${APIMOCKER_BIN_DIR}/pip install -r ${APIMOCKER_DIR}/requirements.txt
${APIMOCKER_BIN_DIR}/python ${APIMOCKER_DIR}/launcher.py
tail -f /home/ampush/services/apimocker/logs/fbadsapi.log
```


Additional steps if testing a new FB Proxy Build:

* Get the new .war file from Ops
```sh
scp -i cm-dev2.pem #####.war root@#####.ampush1.com:/tmp
sudo service jetty stop
cd /opt/jetty/webapps
mv root/ /tmp/root_<date>/
cd /opt/jetty/webapps
mkdir root ; cd root
jar xvf /tmp/#####.war
sudo chown -R jetty:jetty /opt/jetty
sudo service jetty start
```
<date> = current date
#####.war = war file name for new fbproxy build
#####.ampush1.com = AWS instance/hostname


Recommended:  create a local git repository in case any items in bootstrap.properties need to be changed:
```sh
git init
git add .
git commit -m "Original bootstrap.properties"
```

*Verify configuration of /opt/jetty/webapps/root/WEB-INF/classes/bootstrap.properties is in sync with /etc/redis/redis-config.py (i.e. QA databases).  For new builds in particular, you may need to edit this file to use staging instead of master tokens, localhost or hostname.  *You no longer need to reroute the API url to APIMocker; this is done on a test-by-test basis via a context manager.


To run tests and view results (from within AWS instance):
```sh
FBPROXY_QA_DIR=/home/ampush/services/qa/tools/test/fbproxy
$FBPROXY_QA_DIR/run_fbproxy_tests.sh
grep ERROR $FBPROXY_QA_DIR/logs/fbproxy.log
```


End-to-end Selenium tests (to be tested locally):

Run Selenium server:
```sh
java -jar selenium-server-standalone-2.40.0.jar # from directory with jar file
```
Set environment variable (and load balancer variables, if needed) in tools/test/fbproxy/ui/test_fbproxy_manual_actions.py 
```python
AWS_ENV = '#####'
```

In separate shell, run tests in selenium virtualenv from QA repo (from qa/ui):
```sh
workon selenium
./sanities tools/test/fbproxy/ui/test_fbproxy_manual_actions.py --env=#####
```
##### = AWS server (i.e. sb2-qa1)


<hr>

Test Details:

test_error_codes.py: Sends request routes for 79 error codes to API Mocker and verifies that the response triggers the appropriate error handlers

test_error_handlers.py: Triggers 4 error handlers from API Mocker's response and verifies that they behave as expected

test_queue_size.py:  Verifies that the queue size set in bootstrap.properties allows n requests into the queue; additional requests will error.  **There is a known issue with this test, where the queue actually allows n + 2 requests.  See https://ampush.atlassian.net/browse/AIRU-5964.

test_drain_queue: Verifies that queue is emptied when jetty is restarted.

test_inactive_account: Changes the end date of an account to the past in the mysql database and sends manual actions; verifies that the action isn't queued.

test_corrupt_token: Corrupts an account token in the mysql database and sends manual actions; verifies that the action succeeds (is sent with a master token)


Tests make use of a library that includes:

config_bootstrap_properties.py: Class that directly updates values in the bootstrap.properties configuration file for FB Proxy

check_svc_status.py: Script that checks the status of FB Proxy and dependent services and restarts them as needed

parse_requests.py: Script that pulls data from log files

settings.py: Shared variables

<hr>

Disabled for this release:

# test_prioritization: to be tested manually by submitting BAC jobs

# test_throughput: Sends requests to FB Proxy at varying rates and verifies that they're processed in the appropriate amount of time. Turn celery off before running this test (service celeryd stop)

# test_load_balancer: additional configuration required

<hr>

Open tickets related to this suite:

Bug - https://ampush.atlassian.net/browse/AIRU-5964 - queue off-by-2 error (see test_queue_size.py above)

Bug - https://ampush.atlassian.net/browse/AIRU-5965 - inactive accounts

Feature - https://ampush.atlassian.net/browse/AIRU-5976 - increase timeout

<hr>

Additional information:

https://docs.google.com/a/ampushmedia.com/presentation/d/1jUafHcGLAyK2FejYQ9N7lTuwOoaFpnimLudL0UpNUS8/edit?usp=sharing