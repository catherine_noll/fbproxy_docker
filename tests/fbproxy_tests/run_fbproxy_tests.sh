#!/bin/bash

FBPROXY_TMP_DIR=$(mktemp -d)
FBPROXY_BIN_DIR=$FBPROXY_TMP_DIR/bin
FBPROXY_QA_DIR=/home/ampush/services/qa/tools/test/fbproxy
FBPROXY_TESTS=$FBPROXY_QA_DIR/tests
FBPROXY_TEST_LOGS=$FBPROXY_QA_DIR/logs

virtualenv $FBPROXY_TMP_DIR
$FBPROXY_BIN_DIR/pip install -r $FBPROXY_QA_DIR/requirements.txt

mkdir -p $FBPROXY_TEST_LOGS
$FBPROXY_BIN_DIR/python tests/test_error_codes.py >& $FBPROXY_TEST_LOGS/test_error_codes.log
$FBPROXY_BIN_DIR/python tests/test_error_handlers.py >& $FBPROXY_TEST_LOGS/test_error_handlers.log
$FBPROXY_BIN_DIR/python tests/test_queue_size.py >& $FBPROXY_TEST_LOGS/test_queue_size.py

#kill launcher.py
#rm -r $FBPROXY_TMP_DIR
#rm -r $APIMOCKER_TMP_DIR
