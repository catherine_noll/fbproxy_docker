import os
import time
import requests
import datetime
from multiprocessing import Pool

from lib.settings import APIMOCKER_LOGPATH
from lib.settings import FB_API_VERSION
from lib.settings import FBPROXY_LOGPATH
from lib.settings import FBPROXY_PORT


def get_next_line(file_object):
    wait_time = datetime.datetime.now() + datetime.timedelta(seconds=61)
    # f.readline() returns '' at End-Of-File
    while True:
        line = file_object.readline()
        if line:
            return line
        elif datetime.datetime.now() >= wait_time:
            return None
        else:
            time.sleep(0.1)


def parse_line(line, resp_dict):
    line_items = line.split(' ')[2:]
    line = ' '.join(line_items)
    for k in resp_dict.keys():
        if k in line.lower():
            if line not in resp_dict[k]:
                resp_dict[k][line] = 0
            resp_dict[k][line] += 1
            return resp_dict
    else:
        if line not in resp_dict['other_info']:
            resp_dict['other_info'][line] = 0
        resp_dict['other_info'][line] += 1
        return resp_dict


def parse_fbproxy_log(num_reqs, file_object, resp_data=None):
    if resp_data is None:
        resp_data = {'error': {},
                     'retry': {},
                     'throttle': {},
                     'redis_set': {},
                     'other_info': {},
                     'warn': {}}
    while True:
        line = get_next_line(file_object)
        if line:
            line = line.strip()
            parse_line(line, resp_data)
        else:
            return resp_data
    return resp_data


def parse_apimocker_log(num_reqs, file_object, resp_data=None):
    if resp_data is None:
        resp_data = {}
    i = 1
    while i <= num_reqs:
        req = 'req_%d' % i
        line = get_next_line(file_object)
        if line:
            line = line.strip()
            if 'tornado' in line:
                line_items = line.split(' ')
                resp_data[req] = {}
                time_info = (line_items[1] + ' ' + line_items[2])
                resp_data[req]['req_date_time'] = (
                    datetime.datetime.strptime(
                        time_info, '%Y-%m-%d %H:%M:%S.%f'))
                resp_data[req]['req_len'] = line_items[10]
                resp_data[req]['req_status'] = line_items[6]
                i += 1
        else:
            return resp_data
    return resp_data


def modification_time(filename):
    t = os.path.getmtime(filename)
    return datetime.datetime.fromtimestamp(t)


def wait_for_log_start(log_start, logpath):
    last_mod = modification_time(logpath)
    if log_start <= last_mod:
        return
    else:
        time.sleep(5)
        print "waiting for log..."
        wait_for_log_start(last_mod, logpath)


def wait_for_log(log_start, logpath):
    last_mod = modification_time(logpath)
    if log_start == last_mod:
        return
    else:
        time.sleep(5)
        print "waiting for log..."
        wait_for_log(last_mod, logpath)


def verify_resp_ab(ab_resp, num_reqs):
    assert ab_resp['req_stats'] != {}, (
        "ab response was not fully parsed")


def setup_log(log_path):
    os.system('echo > %s' % log_path)
    log_object = open(log_path)
    log_object.seek(0, 2)
    log_start = modification_time(log_path)
    return {'log_object': log_object, 'log_start': log_start}


def get_log_info(log, log_path, num_reqs):
    wait_for_log(log['log_start'], log_path)
    if "jetty" in log_path:
        log_data = parse_fbproxy_log(num_reqs, log['log_object'])
    elif "apimocker" in log_path:
        log_data = parse_apimocker_log(num_reqs, log['log_object'])
    log['log_object'].close()
    log_end = modification_time(log_path)
    log_timing = (log_end - log['log_start']).total_seconds()
    return {'log': log_data, 'timing': log_timing}


def send_batch_read_logs(get_logs=None,
                         req_type='get',
                         num_reqs=10,
                         postdata=None,
                         url=('http://localhost:%s/%s/6017678624990?'
                              'amp_act_id=act_345926612250332&'
                              'amp_method=graph_update_adgroup&'
                              'redownload=1&'
                              'name=Leotest' % (FBPROXY_PORT, FB_API_VERSION))):

    fbproxy_info = None
    apimocker_info = None
    if get_logs is None:
        get_logs = []
    if 'fbproxy' in get_logs:
        fbproxy_log = setup_log(FBPROXY_LOGPATH)
    if 'apimocker' in get_logs:
        apimocker_log = setup_log(APIMOCKER_LOGPATH)

    resp = send_batch(req_type, num_reqs, postdata, url)
    # resp = send_batch_GET(req_type, num_reqs, postdata, url)

    if 'fbproxy' in get_logs:
        fbproxy_info = get_log_info(fbproxy_log, FBPROXY_LOGPATH, num_reqs)
    if 'apimocker' in get_logs:
        apimocker_info = get_log_info(
            apimocker_log, APIMOCKER_LOGPATH, num_reqs)

    resp_type = 'resp_%s' % req_type

    return {resp_type: resp,
            'fbproxy': fbproxy_info,
            'apimocker': apimocker_info
            }


# def send_batch_GET(num_reqs, postdata, url):
#     pool = Pool(num_reqs)
#     responses = pool.map(requests.get, [url] * num_reqs)
#     for resp in responses:
#         print resp.status_code
#     return responses


def send_request(kwargs):
    kwargs = kwargs.copy()
    request_func = getattr(requests, kwargs.pop('req_type'))
    url = kwargs.pop('url')
    return request_func(url, **kwargs)


def generate_send_batch(url, num_reqs, req_type, extra_kwargs):
    kwargs = extra_kwargs.copy()
    kwargs['url'] = url
    kwargs['req_type'] = req_type
    pool = Pool(8)
    responses = pool.map(send_request, [kwargs] * num_reqs)
    return responses


def send_batch(req_type='get',
               num_reqs=1,
               postdata=None,
               url=('http://localhost:%s/6017678624990?'
                    'amp_act_id=act_345926612250332'
                    '&amp_method=graph_update_adgroup'
                    '&redownload=1&'
                    'name=Leotest' % FBPROXY_PORT),
               extra_kwargs=None):
    if extra_kwargs is None:
        extra_kwargs = {}
    responses = generate_send_batch(url, num_reqs, req_type, extra_kwargs)
    for resp in responses:
        print resp.status_code
    return responses
