import subprocess

# Env info
AWS_ENV = subprocess.check_output('hostname').strip()
AWS_LB_ENV = 'sb20-rep1.ampush1.com'
HOST_NAME = $(docker-machine ip dockervm)
# Logs
APIMOCKER_LOGPATH = '/home/ampush/services/apimocker/logs/fbadsapi.log'
FBPROXY_LOGPATH = '/var/log/jetty/server.log'
# Config
BOOTSTRAP_PROPERTIES = '/opt/jetty/webapps/root/WEB-INF/classes/bootstrap.properties'
ORIG_TOKEN = 'CAAHJryxM36QBAPzm7XwOS7ZCw2qcMYpqU7wEKoDTj8cBf1M9pHsjc1t1z2GB5y1S1Q6ucJmBBgYppRXI8DdrXSLjFe2MZA7KXw8ZC708DqFG0OLN24v9QsVpZAWLsKqvhNJkX52lAbhZCDd9QDfPOxSXosTa8n2MJWP1uJ67GxFYKyLx08O9ZAERPbtEHw4jIVkzXKvS7jEuHSfd0DbnBJSCdhWQgAUqIZD'
# Load balance data
LB_ENV = 'sb29-rep1.ampush1.com'
LB_PORT = 80
BALANCED_ENVS = ['sb29-rep1.ampush1.com', 'sb20-rep1.ampush1.com']
# Services
APIMOCKER_PORT = 7777
DJANGO_PORT = 8000
FBPROXY_PORT = 8080
REDIS_PORT = 6379
# API info
FB_API_VERSION = 'v2.4'
API_URL_FB = r'http\://graph.facebook.com/%s' % FB_API_VERSION
API_URL = r'http\://localhost\:7777'
# Errors handled by FB Proxy
ERRORS = {
    'E1487380':
        {'type': 'Error',
         'pause': False,
         'str': '1487380',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Invalid aggregation day for conversion data. The aggregation day must be larger than or equal to 1.',
         'code': 1487380},
    'E1667002':
        {'type': 'Error',
         'pause': False,
         'str': '1667002',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': "This Promotion Can't Be Edited: This can't be edited because you promoted it from your Page. Find the post on your Page to pause, delete, or update budget for this promotion.",
         'code': 1667002},
    'E1487108':
        {'type': 'Error',
         'pause': False,
         'str': '1487108',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Invalid Cities: Please check that the format in which you are specify the cities is correct, and if you specify ids, that they are of the correct type (not, for example, the id of the page for the city; city ids are returned by, e.g., graph.facebook.com/search?type=adcity).',
         'code': 1487108},
    'E1487007':
        {'type': 'Error',
         'pause': False,
         'str': '1487007',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': "Campaign Ended: You can't edit ads in a campaign that has ended. Please create a new ad within this campaign, update the campaign end time, pick another campaign, or create a new campaign.",
         'code': 1487007},
    'E1487006':
        {'type': 'Error',
         'pause': False,
         'str': '1487006',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Invalid Campaign ID: You must specify a campaign, and the campaign specified must belong to the account specified and must not be deleted.',
         'code': 1487006},
    'E803':
        {'type': 'Error',
         'pause': False,
         'str': '803',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Some of the aliases you requested do not exist',
         'code': 803},
    'E1487477':
        {'type': 'Error',
         'pause': False,
         'str': '1487477',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': "Conversion Tracking Pixel Permission Error: The conversion tracking pixel you're trying to use is owned by another user and hasn't been shared with you. Please use a tracking pixel that's been shared with you or create a new one.",
         'code': 1487477},
    'E5000':
        {'type': 'Error',
         'pause': False,
         'str': '5000',
         'handler': 'Retry',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Unknown Error Code',
         'code': 5000},
    'E1487366':
        {'type': 'Error',
         'pause': False,
         'str': '1487366',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Custom Audience Has Been Deleted: Cannot use deleted custom audience. Please choose another audience.',
         'code': 1487366},
    'E1487742':
        {'type': 'Error',
         'pause': False,
         'str': '1487742',
         'handler': 'DisableApp',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'There have been too many calls from this ad-account. Wait a bit and try again.',
         'code': 1487742},
    'E17':
        {'type': 'Error',
         'pause': False,
         'str': '17',
         'handler': 'DisableApp',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'API User Too Many Calls / User request limit reached',
         'code': 17},
    'E1487124':
        {'type': 'Error',
         'pause': False,
         'str': '1487124',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Invalid Connection: You can only specify connections to objects you are an administrator or developer of.',
         'code': 1487124},
    'E1487089':
        {'type': 'Error',
         'pause': False,
         'str': '1487089',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Target Spec Invalid: The target spec is invalid: {reason}',
         'code': 1487089},
    'E1487202':
        {'type': 'Error',
         'pause': False,
         'str': '1487202',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Invalid object - not admin or object not public: The user is not an admin of the object or the object is not publicly accessible.',
         'code': 1487202},
    'E1487301':
        {'type': 'Error',
         'pause': False,
         'str': '1487301',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': "Custom Audience Unavailable: The custom audience you're trying to use hasn't been shared with your ad account. Please create or choose a different custom audience, or ask the owner of the custom audience to let you use it.",
         'code': 1487301},
    'E1487573':
        {'type': 'Error',
         'pause': False,
         'str': '1487573',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'You must specify an object_id for this adcreative type (creative type 27)',
         'code': 1487573},
    'E1487087':
        {'type': 'Error',
         'pause': False,
         'str': '1487087',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Adgroup Create Failed: The Adgroup Create Failed for the following reason: {reason}',
         'code': 1487087},
    'E294':
        {'type': 'Error',
         'pause': False,
         'str': '294',
         'handler': 'Permission',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Managing advertisements requires the extended permission ads_management and an application that is whitelisted to access the Ads API',
         'code': 294},
    'E1487225':
        {'type': 'Error',
         'pause': False,
         'str': '1487225',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Adgroup Creation Limited By Daily Spend: The number of adgroups you can create in a given period of time has a limit determined by your daily spend level. Higher spend levels allow creation of more adgroups. Increase your daily spend limit or create fewer ads per time period.',
         'code': 1487225},
    'E1487015':
        {'type': 'Error',
         'pause': False,
         'str': '1487015',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'The ad creative is invalid',
         'code': 1487015},
    'E1487013':
        {'type': 'Error',
         'pause': False,
         'str': '1487010',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': "Bid Too Low: Your bid is below the minimum for its placement and type. If you don't want your ad to run at the minimum bid rate, please pause it. Otherwise, please increase the bid to be within the suggested range.",
         'code': 1487013},
    'E1359036':
        {'type': 'Error',
         'pause': False,
         'str': '1359036',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Adgroup Create Failed - Too Many Adgroups: The account {account_id} has reached the maximum number of adgroups {max}',
         'code': 1359036},
    'E102':
        {'type': 'Error',
         'pause': False,
         'str': '102',
         'handler': 'Retry',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'API Session / Session key invalid or no longer valid',
         'code': 102},
    'E100':
        {'type': 'Error',
         'pause': False,
         'str': '100',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Invalid parameter',
         'code': 100},
    'E200_299':
        {'type': 'Error',
         'pause': False,
         'str': '200',
         'handler': 'Permission',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'API Permission (Multiple values depending on permission)',
         'code': 200},
    'E1487283':
        {'type': 'Error',
         'pause': False,
         'str': '1487283',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Not Allowed To Use View Tags: Only some partners are allowed to use view tags. Please verify that you are using an approved account.',
         'code': 1487283},
    'E104':
        {'type': 'Error',
         'pause': False,
         'str': '102',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Incorrect signature',
         'code': 104},
    'E10':
        {'type': 'Error',
         'pause': False,
         'str': '10',
         'handler': 'Permission',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'API Permission Denied / Application does not have permission for this action',
         'code': 10},
    'E1487194':
        {'type': 'Error',
         'pause': False,
         'str': '1487194',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Permission Error: Either the object you are trying to access is not visible to you or the action you are trying to take is restricted to certain account types.',
         'code': 1487194},
    'E1634013':
        {'type': 'Error',
         'pause': False,
         'str': '1634013',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Conversion Spec Targets Invalid ID: You are targeting an ID in your conversion spec without admin permissions on that object. {debug_info}',
         'code': 1634013},
    'E1487014':
        {'type': 'Error',
         'pause': False,
         'str': '1487014',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Campaign budget was too low. Please increase the daily budget to at least 2 times the amount of the highest CPC adgroup bid, which is at least {minimum_budget}. You bid {bid}.',
         'code': 1487014},
    'UNKNOWN':
        {'type': 'Error',
         'pause': False,
         'str': '0',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': None,
         'code': 0},
    'E4':
        {'type': 'Error',
         'pause': False,
         'str': '4',
         'handler': 'DisableApp',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'API Too Many Calls / Application request limit reached',
         'code': 4},
    'E1487174':
        {'type': 'Error',
         'pause': False,
         'str': '1487174',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Invalid Image Hash: Invalid Image Hash - {hash}',
         'code': 1487174},
    'E1':
        {'type': 'Error',
         'pause': False,
         'str': '1',
         'handler': 'Retry',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'API Unknown / An unknown error occurred',
         'code': 1},
    'E2':
        {'type': 'Error',
         'pause': False,
         'str': '2',
         'handler': 'Retry',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'API Service',
         'code': 2},
    'E1487010':
        {'type': 'Error',
         'pause': False,
         'str': '1487010',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Ad Bid Too High: Your bid is above the maximum for its type and placement. Please try again with a value below the maximum if you would like to raise your bid.',
         'code': 1487010},
    'E1487391':
        {'type': 'Error',
         'pause': False,
         'str': '1487391',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Cannot Use Syndicated Audiences From Multiple Partners: When specifying partner-created custom audiences for an adgroup, all audiences must be from the same provider.',
         'code': 1487391},
    'E1487246':
        {'type': 'Error',
         'pause': False,
         'str': '1487246',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Campaign Creation Failed: {reason}',
         'code': 1487246},
    'E1487465':
        {'type': 'Error',
         'pause': False,
         'str': '1487465',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': "Invalid CPA Bid: The action type you selected is not allowed for Cost-Per-Action (CPA) bidding: '{action_type}'. Please change your optimization goal or switch your bid type to regular Optimized CPM.",
         'code': 1487465},
    'E1487756':
        {'type': 'Error',
         'pause': False,
         'str': '1487756',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'You have overlapping locations specified in the geo_locations field. Your broader option will override the narrower option, please remove one to proceed.',
         'code': 1487756},
    'E1487506':
        {'type': 'Error',
         'pause': False,
         'str': '1487506',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'The new budget value is too low: The new budget value of {new_budget} is too low. The minimum acceptable is {minimum_budget}.',
         'code': 1487506},
    'E1487472':
        {'type': 'Error',
         'pause': False,
         'str': '1487472',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'This type of page post is not eligible to be promoted. {debug_info}',
         'code': 1487472},
    'E1487396':
        {'type': 'Error',
         'pause': False,
         'str': '1487396',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Failed to update the creative: Failed to update creative {id}. Failed for the following reason: {reason}',
         'code': 1487396},
    'E1487376':
        {'type': 'Error',
         'pause': False,
         'str': '1487376',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Conversion Spec Needed: For ads pointing to an offsite location with a non-zero bid on actions, you must specify the conversion spec.',
         'code': 1487376},
    'E1487256':
        {'type': 'Error',
         'pause': False,
         'str': '1487246',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Targeting declined due to policy: Invalid ads targeting. The targeting spec was declined due to policy restrictions.',
         'code': 1487246},
    'E1359102':
        {'type': 'Error',
         'pause': False,
         'str': '1359102',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Invalid Parameter In Spec: Parameter has invalid value or missing required parameter: {param-value}',
         'code': 1359102},
    'E1349118':
        {'type': 'Error',
         'pause': False,
         'str': '1349118',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': "One or more of the given URLs is not allowed by the Stream post URL security app setting. It must match the Website URL or Canvas URL, or the domain must be a subdomain of one of the App's domains. Read more about Login Security to learn more about app settings related to security.",
         'code': 1349118},
    'E1487908':
        {'type': 'Error',
         'pause': False,
         'str': '1487908',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': "The given placement is incompatible with ad's objective.",
         'code': 1487908},
    'E1487133':
        {'type': 'Error',
         'pause': False,
         'str': '1487133',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Invalid Negative Connections: If you specify negative targeting, you must be the administrator or developer of the objects whose fans you want to specify to exclude. You are not an admin of the following specified connections: {connections}',
         'code': 1487133},
    'E1487424':
        {'type': 'Error',
         'pause': False,
         'str': '1487424',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Adgroup Needs Exclusion Targeting: This adgroup cannot be created with the targeting spec you selected. Your ad has a conversion objective that users can only do once, so you must target the ad at those users who have not already converted. If you are using the Facebook API, consider including the following string in your targeting spec: {exclusion-targeting-spec}.',
         'code': 1487424},
    'E1487090':
        {'type': 'Error',
         'pause': False,
         'str': '1487090',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Adgroup Edit Failed - Spec Errors: {error}',
         'code': 1487090},
    'E1487172':
        {'type': 'Error',
         'pause': False,
         'str': '1487172',
         'handler': 'Retry',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Could not save creative',
         'code': 1487172},
    'E1609005':
        {'type': 'Error',
         'pause': False,
         'str': '1609005',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Error Posting Link',
         'code': 1609005},
    'E1487390':
        {'type': 'Error',
         'pause': False,
         'str': '1487390',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Adcreative Create Failed: The Adcreative Create Failed for the following reason: {reason}',
         'code': 1487390},
    'E1487211':
        {'type': 'Error',
         'pause': False,
         'str': '1487202',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Invalid URL For Creative Destination: Creative must have a valid destination URL, and if attached object is page, destination must match page.',
         'code': 1487211},
    'E1487915':
        {'type': 'Error',
         'pause': False,
         'str': '1487915',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'A finalized prediction order can not be cancelled. You can create a new prediction or delete the campaign.',
         'code': 1487915},
    'E1487882':
        {'type': 'Error',
         'pause': False,
         'str': '1487882',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'The given reservation in a reach and frequency campaign is not finalized yet.',
         'code': 1487882},
    'E1487881':
        {'type': 'Error',
         'pause': False,
         'str': '1487881',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'The given prediction id in a reach and frequency campaign is not reserved.',
         'code': 1487881},
    'E1487887':
        {'type': 'Error',
         'pause': False,
         'str': '1487887',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'The given page types are not supported by the inventory manager.',
         'code': 1487887},
    'E1487886':
        {'type': 'Error',
         'pause': False,
         'str': '1487885',
         'handler': 'Retry',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'This indicates the operation fails for an unknown reason.',
         'code': 1487886},
    'E1487885':
        {'type': 'Error',
         'pause': False,
         'str': '1487885',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'The prediction in a reach and frequency campaign is already reserved.',
         'code': 1487885},
    'E1487884':
        {'type': 'Error',
         'pause': False,
         'str': '1487884',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'The given reservation in a reach and frequency campaign is already finalized',
         'code': 1487884},
    'E506':
        {'type': 'Error',
         'pause': False,
         'str': '506',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Duplicate Post',
         'code': 506},
    'E1487199':
        {'type': 'Error',
         'pause': False,
         'str': '1487199',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Ad targeting does not match targeting of the story: The targeting specified for this ad is not compatible with the story being boosted. Check the privacy and language/country targeting of the story you are trying to sponsor.',
         'code': 1487199},
    'E2606':
        {'type': 'Error',
         'pause': False,
         'str': '2606',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Unable to display a preview of this ad',
         'code': 2606},
    'E2607':
        {'type': 'Error',
         'pause': False,
         'str': '2607',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'The given currency is invalid for usage with ads.',
         'code': 2607},
    'E1487861':
        {'type': 'Error',
         'pause': False,
         'str': '1487861',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Deleted Ad Sets may only contain deleted Ads.',
         'code': 1487861},
    'E1487860':
        {'type': 'Error',
         'pause': False,
         'str': '1487860',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Archived Ad Sets may only contain archived or deleted Ads.',
         'code': 1487860},
    'E190':
        {'type': 'Error',
         'pause': False,
         'str': '190',
         'handler': 'Retry',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Invalid OAuth 2.0 Access Token',
         'code': 190},
    'E1487065':
        {'type': 'Error',
         'pause': False,
         'str': '1487065',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': "Adgroup Missing Targeting Spec: Adgroup spec needs a targeting spec. Please use targeting field to specify what audience the ad should be shown to. Field 'countries' is required; all others are optional.",
         'code': 1487065},
    'E1487244':
        {'type': 'Error',
         'pause': False,
         'str': '1487244',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Campaign Update Failed: Campaign {campaign_id}: {reason}',
         'code': 1487244},
    'E1487559':
        {'type': 'Error',
         'pause': False,
         'str': '1487559',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Adgroup Update Failed {reason}',
         'code': 1487559},
    'E1487558':
        {'type': 'Error',
         'pause': False,
         'str': '1487558',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'No supported fields were provided for updating the adgroup',
         'code': 1487558},
    'E1487346':
        {'type': 'Error',
         'pause': False,
         'str': '1487346',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'The post does not belong to the specified Page: Post to be promoted does not belong to the Page specified. The post belongs to {otherDestination}; try using this as the destination or using a post that belongs to the specified Page. Specified Page: {destination}.',
         'code': 1487346},
    'E2615':
        {'type': 'Error',
         'pause': False,
         'str': '2615',
         'handler': 'Retry',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Invalid call to update this adaccount',
         'code': 2615},
    'E1487557':
        {'type': 'Error',
         'pause': False,
         'str': '1487557',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'No supported fields were provided for updating the campaign',
         'code': 1487557},
    'E1487790':
        {'type': 'Error',
         'pause': False,
         'str': '1487790',
         'handler': 'Default',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'The promoted object is invalid: it could have been deleted or never existed.',
         'code': 1487790},
    'E1487485':
        {'type': 'Error',
         'pause': False,
         'str': '1487485',
         'handler': 'DisableApp',
         'subcode': False,
         'retry': False,
         'suspend token': False,
         'msg': 'Too many async requests',
         'code': 1487485}
}
