import os
import redis
import requests
import subprocess
import time
from requests.exceptions import ConnectionError

from lib.settings import APIMOCKER_LOGPATH
from lib.settings import APIMOCKER_PORT
from lib.settings import AWS_ENV
from lib.settings import FBPROXY_LOGPATH
from lib.settings import FBPROXY_PORT
from lib.settings import HOST_NAME
from lib.settings import REDIS_PORT


def handle_connection_error(function):
    def connection_error():
        function_name = function.func_name.split('_')[-1]
        try:
            function()
            print "%s is up" % function_name
            return True
        except ConnectionError:
            print "%s check connection" % function_name
            return False
    return connection_error


def check_individual_services():
    check_apimocker()
    check_fbproxy()
    check_redis()
    check_django()


def check_default_services():
    check_individual_services()
    check_proxy_to_mocker()


@handle_connection_error
def check_apimocker():
    request_url = 'http://%s:%i' % (HOST_NAME, APIMOCKER_PORT)
    resp = requests.get(request_url)
    resp_status = resp.status_code
    assert resp_status in {200, 400, 405}, "APIMocker is down"


@handle_connection_error
def check_fbproxy():
    request_url = 'http://%s:%i' % (AWS_ENV, FBPROXY_PORT)
    resp = requests.get(request_url)
    resp_status = resp.status_code
    assert resp_status in {200, 400}, "FBProxy is down"


@handle_connection_error
def check_redis():
    conn = redis.Connection()
    assert conn.db == 0 and conn.port == REDIS_PORT, "Redis is down"


@handle_connection_error
def check_django():
    url = 'https://%s' % AWS_ENV
    resp = requests.get(url, verify=False)
    resp_status = resp.status_code
    assert resp_status in {200, 400}, "AMP is down"


def get_apimocker_lastupdated():
    return time.ctime(os.path.getmtime(APIMOCKER_LOGPATH))


def get_fbproxy_lastupdated():
    return time.ctime(os.path.getmtime(FBPROXY_LOGPATH))


def check_proxy_to_mocker():
    req_time = time.ctime()
    requests.get('http://%s:%s/6017678624990'
                 '?amp_act_id=act_345926612250332'
                 '&amp_method=graph_update_adgroup'
                 '&redownload=1&name=Leotest' % (HOST_NAME, FBPROXY_PORT))
    apimocker_lastupdated = get_apimocker_lastupdated()
    fbproxy_lastupdated = get_fbproxy_lastupdated()
    i = 0
    while True:
        if (req_time <= apimocker_lastupdated and
                req_time <= fbproxy_lastupdated):
            print "fbproxy is routed to apimocker"
            return True
        elif i > 10:
            print "apimocker log did not register fbproxy request"
            return False
        else:
            print "checking fbproxy to apimocker:  waiting..."
            time.sleep(2)
            apimocker_lastupdated = get_apimocker_lastupdated()
            fbproxy_lastupdated = get_fbproxy_lastupdated()
            i += 1


def check_proxy_to_fb():
    req_time = time.ctime()
    resp = requests.get('http://%s:%s/6017678624990' % (HOST_NAME, FBPROXY_PORT))
    apimocker_lastupdated = get_apimocker_lastupdated()
    fbproxy_lastupdated = get_fbproxy_lastupdated()
    if ((req_time > apimocker_lastupdated) and (req_time <= fbproxy_lastupdated) and (resp.status_code in {200, 400})):
            print "WARNING:  fbproxy is routed to facebook"
            return True
    else:
        raise ConnectionError('Facebook was not reached')


def restart_fbproxy():
    stop_jetty_line = ['sudo', 'service', 'jetty', 'stop']
    subprocess.Popen(stop_jetty_line, stdout=subprocess.PIPE).communicate()
    proxy_status = check_fbproxy()
    assert proxy_status is False, (
        "Proxy should have been stopped but is running")
    start_jetty_line = ['sudo', 'service', 'jetty', 'start']
    subprocess.Popen(start_jetty_line, stdout=subprocess.PIPE)
    # Allot wait time for FBProxy to start once Jetty is up
    i = 0
    while True:
        new_proxy_status = check_fbproxy()
        if new_proxy_status is True:
            break
        elif i >= 10:
            raise ConnectionError('Max retries for FB Proxy connection')
        elif new_proxy_status is False:
            time.sleep(2)
            new_proxy_status = check_fbproxy()
            i += 1
    assert new_proxy_status, "Proxy should be up but is stopped"


if __name__ == '__main__':
    check_default_services()
