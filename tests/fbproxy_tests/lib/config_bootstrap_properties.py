import subprocess
from contextlib import contextmanager
import lib.check_svc_status as SVC

from lib.settings import API_URL
from lib.settings import API_URL_FB
from lib.settings import BOOTSTRAP_PROPERTIES
from lib.settings import ORIG_TOKEN


def replace_string(string, replacement, file):
    with open(file) as f:
        contents = f.read()
    contents = contents.replace(string, replacement)
    with open(file, 'w') as f:
        f.write(contents)


class ConfigBootstrapProperties(object):
    _property_names = {
        'wps': 'proxy.wps',
        'token': 'access.token.master.staging.1',
        'qsize': 'queue.priority.qsize',
        'api_url': 'facebook.api.url',
    }

    def __init__(self):
        values = {name: self._read_value(name) for name in self._property_names}
        self.set_values(**values)

    def apply(self, check_proxy_routing=False):
        for name, value in self.get_values().items():
            self._write_value(name, value)
        SVC.restart_fbproxy()
        SVC.check_individual_services()
        if self.api_url == API_URL and check_proxy_routing:
            SVC.check_proxy_to_mocker()
        elif self.api_url == API_URL_FB and check_proxy_routing:
            SVC.check_proxy_to_fb()
        print "Update to bootstrap.properties file: %s" % self.get_values().items()

    @classmethod
    def _write_value(cls, name, value):
        file_name = cls._property_names[name]
        current_val = cls._read_value(name)
        current = '%s=%s' % (file_name, current_val)
        replacement = '%s=%s' % (file_name, value)
        replace_string(current, replacement, BOOTSTRAP_PROPERTIES)
        check_val = cls._read_value(name)
        assert check_val == str(value), \
            "Bootstrap.properties %s should be %s, but is %s" % (
                name, value, check_val)

    @classmethod
    def _read_value(cls, name):
        file_name = cls._property_names[name]
        cmd_line = ['grep', file_name, BOOTSTRAP_PROPERTIES]
        output = subprocess.check_output(cmd_line)
        value = output.split('=')[-1].strip()
        return value

    def set_values(self, **kwargs):
        for val_name, val in kwargs.items():
            setattr(self, val_name, val)

    def get_values(self):
        data = {}
        for prop_name in self._property_names:
            data[prop_name] = getattr(self, prop_name)
        return data


@contextmanager
def property_values(**kwargs):
    cbp = ConfigBootstrapProperties()
    original_values = cbp.get_values()
    cbp.set_values(**kwargs)
    cbp.apply()
    try:
        yield
    finally:
        cbp.set_values(**original_values)
        cbp.apply()
