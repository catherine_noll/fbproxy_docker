import lib.parse_requests as PR
from lib.config_bootstrap_properties import property_values
from lib.config_bootstrap_properties import ConfigBootstrapProperties as CBP  # noqa
from lib.settings import API_URL
from lib.settings import API_URL_FB

N = 100


def verify_assertion_throughput(function):
    def assertion_throughput():
        function_name = function.func_name
        if CBP._read_value('api_url') == API_URL_FB:
            print "ERROR:  FBProxy is routed to FB"
            return
        resp = function()
        print "testing %s ......" % function_name
        if 'fail' in function_name:
            result = test_negative_case(resp)
        else:
            result = test_positive_case(resp)
        print "OK"
        return result
    return assertion_throughput


def get_observed_timing(resps):
    times = []
    for resp in resps['apimocker']['log'].keys():
        times.append(resps['apimocker']['log'][resp]['req_date_time'])
    total_time = (max(times) - min(times)).total_seconds()
    avg_time = total_time / len(resps['resp_get'])
    return avg_time


def get_stdev(num_reqs, wps):
    stdev = ((num_reqs ** (1 / 2.0)) / wps) / num_reqs
    return stdev


def test_positive_case(resp):
    wps = int(CBP._read_value('wps'))
    resps = len(resp['resp_get'])
    num_reqs_apimocker = len(resp['apimocker']['log'])
    obs_avg_time = get_observed_timing(resp)
    exp_mean = 1.0 / wps
    accepted_deviation = get_stdev(N, wps) * 2
    accepted_min = exp_mean - accepted_deviation
    accepted_max = exp_mean + accepted_deviation
    assert num_reqs_apimocker == resps, (
        "Only %s of %s requests hit API Mocker") % (
        num_reqs_apimocker, resps)
    assert accepted_min <= obs_avg_time <= accepted_max, (
        "WPS set to %s but requests were processed at %s") % (
        wps, 1 / obs_avg_time)
    return {'wps': wps,
            'observed': obs_avg_time,
            'expected': exp_mean,
            'accepted_min': accepted_min,
            'accepted_max': accepted_max}


def test_negative_case(resp):
    wps = CBP._read_value('wps')
    resps = len(resp['resp_get'])
    assert resps == N, "Only %s responses of %s sent" % (resps, N)
    assert resp['apimocker']['log'] == {}, (
        "No data should be written to API Mocker log at 0 wps!")
    return {'wps': wps, 'observed': {}, 'expected': {}}


# Verify that requests to FB Proxy are routed to API Mocker at 1 WPS
@verify_assertion_throughput
def test_throughput_base():
    with property_values(wps=1):
        kwargs = {'num_reqs': N, 'get_logs': 'apimocker'}
        resp = PR.send_batch_read_logs(**kwargs)
    return resp


# Reduce throughput to 0, verify no requests are routed to API Mocker
@verify_assertion_throughput
def test_throughput_fail():
    with property_values(wps=0):
        kwargs = {'num_reqs': N, 'get_logs': 'apimocker'}
        resp = PR.send_batch_read_logs(**kwargs)
    return resp


# Verify that requests to FB Proxy are routed to API Mocker at 5 WPS
# (rate currently used in production)
@verify_assertion_throughput
def test_throughput_low():
    with property_values(wps=5):
        kwargs = {'num_reqs': N, 'get_logs': 'apimocker'}
        resp = PR.send_batch_read_logs(**kwargs)
    return resp


# Verify that requests to FB Proxy are routed to API Mocker at 10 WPS
@verify_assertion_throughput
def test_throughput_current():
    with property_values(wps=10):
        kwargs = {'num_reqs': N, 'get_logs': 'apimocker'}
        resp = PR.send_batch_read_logs(**kwargs)
    return resp


# Verify that requests to FB Proxy are routed to API Mocker at 20 WPS
@verify_assertion_throughput
def test_throughput_increase():
    with property_values(wps=20):
        kwargs = {'num_reqs': N, 'get_logs': 'apimocker'}
        resp = PR.send_batch_read_logs(**kwargs)
    return resp


# Find the max number of requests that can be processed
@verify_assertion_throughput
def test_throughput_max():
    with property_values(wps=50):
        kwargs = {'num_reqs': N, 'get_logs': 'apimocker'}
        resp = PR.send_batch_read_logs(**kwargs)
    return resp


def compare_resps():
    base = test_throughput_base()
    prod = test_throughput_low()
    current = test_throughput_current()
    increase = test_throughput_increase()
    max_t = test_throughput_max()
    return base, current, prod, increase, max_t


if __name__ == '__main__':
    with property_values(api_url=API_URL):
        print "Testing throughput"
        test_throughput_base()
        test_throughput_fail()
        test_throughput_low()
        test_throughput_current()
        test_throughput_increase()
        test_throughput_max()
