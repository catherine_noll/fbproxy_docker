import datetime
import json
import subprocess
import time
import unittest

from nose.plugins.attrib import attr
from lib.common import SeleniumMixin
from lib.pages.manual_actions_page import ManualActionsPage
from lib.pages.manage_actions_page import ManageActionsPage

FBPROXY_LOGPATH = '/var/log/jetty/server.log'
SCRIPT_DIRECTORY = '/home/ampush/services/qa/tools/test/fbproxy/scripts'
AWS_ENV = 'sb2-qa1'
LB_ENV = 'sb29-rep1.ampush1.com'
LB_PORT = 80
BALANCED_ENVS = ['sb29-rep1', 'sb20-rep1']
READ_QUEUE = 'drain_queue.py'


class FBProxyTests(SeleniumMixin, unittest.TestCase):

    @classmethod
    def ssh_run_python_script(cls,
                              script_name,
                              script_dir=SCRIPT_DIRECTORY,
                              env=AWS_ENV):
        return cls.ssh_remote_command([
            'python',
            '%s/%s' % (script_dir, script_name),
        ], env=env)

    @classmethod
    def ssh_remote_command(cls, args, env=AWS_ENV):
        call_args = ['ssh', '%s.ampush1.com' % env]
        call_args.extend(args)
        return subprocess.check_output(call_args)

    @classmethod
    def check_load_balance(cls,
                           envs=BALANCED_ENVS,
                           script_dir=SCRIPT_DIRECTORY):
        load_info = {}
        for env in envs:
            env_data = cls.ssh_run_python_script(
                'get_queue_info.py', script_dir, env=env)
            load_info[env] = json.loads(env_data)
        return load_info

    @classmethod
    def restart_jetty(cls, env=AWS_ENV):
        return cls.ssh_run_python_script('restart_fbproxy.py', env=env)

    # def load_data(self):
    #     ads = self.nav.navigate_to_ad_sets()
    #     ads.validate_data_present()
    #     self.manual_actions = ManualActionsPage(self.driver)

    def select_all_ads_adsets_campaigns(self):
        self.driver.find_element_by_id('checkAll').click()

    def select_first_ad_adset_campaign(self):
        self.driver.find_elements_by_css_selector(
            '#grid .k-grid-content tr')[0].click()

    def get_job_by_id(self, job_id):
        rows = self.driver.find_element_by_id(
            'grid').find_elements_by_tag_name('tr')
        for row in range(len(rows)):
            rows = self.driver.find_element_by_id(
                'grid').find_elements_by_tag_name('tr')
            job_id_column = rows[row].text.split()[0]
            if str(job_id) == job_id_column:
                rows[row].click()
                break

    def get_newest_job(self):
        self.driver.find_element_by_id(
            'grid').find_elements_by_tag_name('tr')[1].click()

    def switch_windows(self):
        time.sleep(5)
        acct_window = self.driver.window_handles[0]
        self.driver.switch_to_window(acct_window)

    def parse_date(self, date):
        date_formatted = date.strip().split('\n')[-1]
        return datetime.datetime.strptime(date_formatted, '%Y-%m-%d %H:%M:%S')

    def perform_manual_action(self, num=1000):
        ads = self.nav.navigate_to_ads()
        ads.select_contract('Ampush Recruiting')
        time.sleep(20)
        job_rows = self.driver.find_elements_by_css_selector(
            '#grid .k-grid-content tr')
        assert job_rows, "No rows in manager!"
        self.restart_jetty()
        self.apply_manual_action(num)

    def apply_manual_action(self, num):
        manage_actions = ManageActionsPage(self.driver)
        manual_actions = ManualActionsPage(self.driver)
        min_bid = 0
        max_bid = 2
        update_formula = '=bid-.02'
        manage_actions.apply_status_filter('PAUSED')
        manage_actions.apply_metric_filter('Bid', 1, 9, 1)
        manage_actions.apply_metric_filter('Budget', 20, 0, 2)
        manage_actions.change_bid_value(update_formula, min_bid, max_bid)
        if num > 1:
            self.select_all_ads_adsets_campaigns()
        elif num == 1:
            self.select_first_ad_adset_campaign()
        manage_actions.submit_manual_action()
        job_id = manual_actions.get_job_id_from_modal
        return job_id

    def apply_one_manual_action(self):
        manage_actions = ManageActionsPage(self.driver)
        manual_actions = ManualActionsPage(self.driver)
        min_bid = 0
        max_bid = 2
        update_formula = '=bid+.02'
        manage_actions.change_bid_value(update_formula, min_bid, max_bid)
        self.select_first_ad_adset_campaign()
        manage_actions.submit_manual_action()
        job_id = manual_actions.get_job_id_from_modal
        return job_id

    def verify_manual_action(self, job_id):
        manual_actions = ManualActionsPage(self.driver)
        manual_actions.nav_to_activity_log()
        time.sleep(5)
        self.get_job_by_id(job_id)

    def upload_build_sheet(self):
        bac = self.nav.navigate_to_bac()
        modal = bac.start_workflow()
        modal.select_contract('BZ - Sales')
        modal.select_account('Ampush Ads')
        modal.upload_csv()
        modal.upload_image_zip()
        modal.select_create_step()
        modal.begin_validation()
        time.sleep(30)
        self.driver.find_element_by_link_text('Refresh').click()
        bac.confirm_upload()

    def bulk_ad_creator_validate(self):
        '''
        Test upload build sheet and create ads
        '''
        self.upload_build_sheet()
        self.driver.find_element_by_link_text('Refresh').click()
        log = self.nav.navigate_to_activity_log()
        time.sleep(5)
        log.select_contract('BZ - Sales')
        log.select_account('kraven')
        start_dt = datetime.datetime.now() - datetime.timedelta(hours=1)
        end_dt = datetime.datetime.now()
        log.set_date_time(start_dt, end_dt)
        log = log.update()
        log.wait_for_bac_validation(120)
        self.nav.logout()

# TESTS

    @attr('regression', 'sanity')
    def test_drain_queue(self):
        self.restart_jetty()
        job = self.perform_manual_action()
        time.sleep(10)
        self.ssh_run_python_script(
            script_name='drain_queue.py',
            script_dir=SCRIPT_DIRECTORY)
        self.verify_manual_action(job)

    @attr('regression', 'sanity')
    def test_inactive_account(self):
        # Submit a job and verify that it's added to the queue
        job = self.perform_manual_action(num=1000)
        time.sleep(15)
        resp = self.ssh_run_python_script(
            script_name='get_queue_info.py',
            script_dir=SCRIPT_DIRECTORY)
        assert int(json.loads(resp)['total_in_queue']) > 1, (
            "No jobs added to queue")
        self.verify_manual_action(job)
        self.restart_jetty()
        # Confirm queue was drained on jetty restart
        resp = self.ssh_run_python_script(
            script_name='get_queue_info.py',
            script_dir=SCRIPT_DIRECTORY)
        assert int(json.loads(resp)['total_in_queue']) == 0
        # Set account to past date in mysql
        exp = self.ssh_remote_command([
            'sh %s/%s' % (SCRIPT_DIRECTORY, 'acct_exp.sh')])
        exp_obj = self.parse_date(exp)
        curr_date = datetime.datetime.now()
        assert curr_date > exp_obj, (
            "Account should be expired but is active (%s)") % exp_obj
        self.switch_windows()
        time.sleep(5)
        # Restart jetty to simulate pokeproxy.py
        self.restart_jetty()
        # Submit manual actions to expired account
        job = self.apply_manual_action(num=1000)
        time.sleep(15)
        resp = self.ssh_run_python_script(
            script_name='get_queue_info.py',
            script_dir=SCRIPT_DIRECTORY)
        assert int(json.loads(resp)['total_in_queue']) == 0, (
            "Jobs were queued for inactive account")
        # Reset account to future date
        reset = self.ssh_remote_command([
            'sh %s/%s' % (SCRIPT_DIRECTORY, 'acct_reset.sh')])
        reset_obj = self.parse_date(reset)
        curr_date = datetime.datetime.now()
        assert curr_date < reset_obj, (
            "Account should be active but is expired (%s)") % reset_obj

    @attr('regression', 'sanity')
    def test_corrupt_token(self):
        # Submit a job and verify that it's added to the queue
        job = self.perform_manual_action(num=1000)
        time.sleep(10)
        resp = self.ssh_run_python_script(
            script_name='get_queue_info.py',
            script_dir=SCRIPT_DIRECTORY)
        assert int(json.loads(resp)['total_in_queue']) > 1
        self.verify_manual_action(job)
        self.restart_jetty()
        # Confirm queue was drained on jetty restart
        resp = self.ssh_run_python_script(
            script_name='get_queue_info.py',
            script_dir=SCRIPT_DIRECTORY)
        assert int(json.loads(resp)['total_in_queue']) == 0
        # Get original token value
        orig_token = self.ssh_remote_command([
            'sh %s/%s' % (SCRIPT_DIRECTORY, 'token_return.sh')])
        # Corrupt the token and restart jetty to simulate pokeproxy.py
        corrupt_token = self.ssh_remote_command([
            'sh %s/%s' % (SCRIPT_DIRECTORY, 'token_corrupt.sh')])
        self.restart_jetty()
        assert orig_token != corrupt_token, "Token should have been corrupted but is the same as original"
        self.switch_windows()
        # Wipe jetty log prior to parsing
        self.ssh_remote_command(['cp /dev/null %s' % FBPROXY_LOGPATH])
        # Submit new job with corrupted token
        job = self.apply_manual_action(num=1)
        resp = self.ssh_run_python_script(
            script_name='get_queue_info.py',
            script_dir=SCRIPT_DIRECTORY)
        # Verify job ultimately succeeds
        self.verify_manual_action(job)
        time.sleep(10)
        # Reset token to original value
        restore = self.ssh_remote_command([
            'sh %s/%s' % (SCRIPT_DIRECTORY, 'token_restore.sh')])
        assert orig_token == restore, "Token should be restored"

    # @attr('regression', 'sanity')
    # def test_load_balancer(self):
    #     # Submit a job and verify that it's added to the queue
    #     job = self.perform_manual_action(num=1000)
    #     time.sleep(10)
    #     load_data = self.check_load_balance(BALANCED_ENVS)
    #     assert int(load_data[BALANCED_ENVS[0]]['total_in_queue']) > 1
    #     assert int(load_data[BALANCED_ENVS[1]]['total_in_queue']) > 1
    #     # Restart one proxy and confirm that it's drained
    #     # and new jobs are routed to other FB Proxy
    #     self.restart_jetty(env=BALANCED_ENVS[0])
    #     self.verify_manual_action(job)
    #     self.switch_windows()
    #     job = self.apply_manual_action(num=1000)
    #     load_data = self.check_load_balance(BALANCED_ENVS)
    #     assert int(load_data[BALANCED_ENVS[0]]['total_in_queue']) == 0
    #     assert int(load_data[BALANCED_ENVS[1]]['total_in_queue']) > 1
    #     # Restart other proxy and confirm that it's drained
    #     # and new jobs are routed to first FB Proxy
    #     self.restart_jetty(env=BALANCED_ENVS[1])
    #     self.switch_windows()
    #     job = self.apply_manual_action(num=1000)
    #     load_data = self.check_load_balance(BALANCED_ENVS)
    #     assert int(load_data[BALANCED_ENVS[0]]['total_in_queue']) > 1
    #     assert int(load_data[BALANCED_ENVS[1]]['total_in_queue']) == 0

    # @attr('regression', 'sanity')
    # def test_queue_p1(self):
    #     # Submit an UPDATE job and verify that it's added to the P1 queue
    #     job = self.perform_manual_action(num=1000)
    #     time.sleep(15)
    #     resp = self.ssh_run_python_script(
    #         script_name='get_queue_info.py',
    #         script_dir=SCRIPT_DIRECTORY)
    #     assert int(json.loads(resp)['p1_in_queue']) > 1, (
    #         "No jobs added to queue")
    #     self.verify_manual_action(job)

    # @attr('regression', 'sanity')
    # def test_queue_p2(self):
    #     # Submit a DELETE job and verify that it's added to the P2 queue
    #     num = 10
    #     schactions = []
    #     for i in range(num):
    #         schaction = self.create_schaction()
    #         schactions.append(schaction)
    #     time.sleep(15)
    #     import ipdb; ipdb.set_trace()
    #     for i in schactions:
    #         self.delete_schaction(num)

    #     resp = self.ssh_run_python_script(
    #         script_name='get_queue_info.py',
    #         script_dir=SCRIPT_DIRECTORY)
    #     assert int(json.loads(resp)['p2_in_queue']) > 1, (
    #         "No jobs added to queue")

    # @attr('regression', 'sanity')
    # def test_queue_p3(self):
        # Submit a CREATE job and verify that it's added to the P3 queue
        # self.upload_build_sheet()
        # job = self.perform_manual_action(num=1000)
        # time.sleep(15)
        # resp = self.ssh_run_python_script(
        #     script_name='get_queue_info.py',
        #     script_dir=SCRIPT_DIRECTORY)
        # assert int(json.loads(resp)['p3_in_queue']) > 1, (
        #     "No jobs added to queue")
        # self.verify_manual_action(job)
