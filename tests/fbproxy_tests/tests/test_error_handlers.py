import datetime
import lib.check_svc_status as SVC
import lib.parse_requests as PR
from lib.config_bootstrap_properties import property_values
from lib.settings import API_URL
from lib.settings import FB_API_VERSION

base_url = ('http://localhost:9090/%s/6017678624990'
            '?amp_act_id=act_345926612250332'
            '&amp_method=graph_update_adgroup'
            '&redownload=1&error_type=' % FB_API_VERSION)
default_error = 'E1487301'
retry_error = 'E5000'
disable_error = 'E1487742'
permissions_error = 'E294'


def test_default_handler():
    SVC.restart_fbproxy()
    url = '%s%s' % (base_url, default_error)
    kwargs = {'num_reqs': 1, 'url': url, 'get_logs': 'fbproxy'}
    print "Testing default handler..."
    resp = PR.send_batch_read_logs(**kwargs)
    error = str(resp['fbproxy']['log']['error'].keys())
    assert 'DefaultApplicationErrorHandler' in error, (
        "DefaultApplicationErrorHandler was not triggered")
    print "OK"


def test_retry_handler():
    SVC.restart_fbproxy()
    url = '%s%s' % (base_url, retry_error)
    kwargs = {'num_reqs': 1, 'url': url, 'get_logs': 'fbproxy'}
    print "Testing retry handler..."
    resp = PR.send_batch_read_logs(**kwargs)
    retries = len(resp['fbproxy']['log']['retry']) - 2
    retry_str = str(resp['fbproxy']['log']['retry'].keys())
    assert 'RetryWithMasterTokenErrorHandler' in retry_str, (
        "RetryWithMasterTokenErrorHandler was not triggered")
    assert retries >= 4, (
        "FBProxy should attempt up to 5 retries, but attempted %s") % retries
    print "OK"


def test_disable_handler():
    SVC.restart_fbproxy()
    url = '%s%s' % (base_url, disable_error)
    # url_default = '%s%s' % (base_url, default_error)
    kwargs = {'num_reqs': 1, 'url': url, 'get_logs': 'fbproxy'}
    print "Testing disable handler trigger..."
    start = datetime.datetime.now()
    resp_disable = PR.send_batch_read_logs(**kwargs)
    end = datetime.datetime.now()
    total_time = (end - start).total_seconds()
    error = str(resp_disable['fbproxy']['log']['error'].keys())
    info = str(resp_disable['fbproxy']['log']['other_info'].keys())
    assert 'DisableAppErrorHandler' in error, (
        "DisableAppErrorHandler was not triggered")
    assert 'Enabling Application' in info, (
        "'Enabling Application' message was not triggered "
        "after app was disabled")
    assert total_time >= 60, (
        "App should be disabled for 1 minute before proceeding, "
        "was only paused for %s seconds") % total_time
    print "OK"


def test_noperm_handler():
    SVC.restart_fbproxy()
    url = '%s%s' % (base_url, permissions_error)
    kwargs = {'num_reqs': 1, 'url': url, 'get_logs': 'fbproxy'}
    print "Testing no permissions handler..."
    resp = PR.send_batch_read_logs(**kwargs)
    error = str(resp['fbproxy']['log']['error'].keys())
    assert 'NoPermissionErrorHandler' in error, (
        "NoPermissionErrorHandler was not triggered")
    print "OK"


if __name__ == '__main__':
    with property_values(wps=5, api_url=API_URL):
        print "Testing error handlers"
        test_default_handler()
        test_disable_handler()
        test_retry_handler()
        test_noperm_handler()
