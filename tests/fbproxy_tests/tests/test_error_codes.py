import datetime
import json
import redis
import subprocess
import time
from requests.exceptions import ConnectionError

import lib.parse_requests as PR
from lib.config_bootstrap_properties import property_values
from scripts.read_fbproxy_log import read_fbproxy_log

from lib.settings import AWS_ENV
from lib.settings import API_URL
from lib.settings import ERRORS
from lib.setings import FB_API_VERSION
from lib.settings import FBPROXY_LOGPATH


def test_error_codes():
    error_not_triggered = {'Default': [],
                           'Permissions': [],
                           'DisableApp': [],
                           'Retry': []}
    for error in ERRORS.keys():
        error_info = ERRORS[error]
        print "Testing %s...." % error
        error_code = error_info['code']
        error_msg = error_info['msg']
        error_handler = error_info['handler']
        subprocess.check_output([
            'ssh', AWS_ENV, 'cp', '/dev/null', FBPROXY_LOGPATH])
        resp = send_req(error)
        time.sleep(2)
        if error_handler == "Retry":
            time.sleep(20)
            fbproxy_log = read_fbproxy_log()
            assert error_handler in str(fbproxy_log['retry']), (
                "Error handler %s was not triggered" % error_handler)
            print "OK"
            continue
        fbproxy_log = read_fbproxy_log()
        if error_code == 0:
            continue
        else:
            assert error_handler in str(fbproxy_log['error']), (
                "Error handler %s was not triggered" % error_handler)
        resp_code, resp_msg = get_data(resp, datetime.datetime.now())
        assert resp_code == error_code, (
            "Expecting code %s but got %s" % (error_code, resp_code))
        assert resp_msg == error_msg, (
            "Expecting msg %s but got %s" % (error_msg, resp_msg))
        print "OK"
    return error_not_triggered


def send_req(error):
    url = ('http://localhost:9090/%s/6017678624990?'
           'amp_act_id=act_345926612250332&'
           'amp_method=graph_update_adgroup&'
           'redownload=1&error_type=%s') % (FB_API_VERSION, error)
    resp = PR.send_batch(num_reqs=1, url=url)[0]
    return resp


def get_data(resp, start_time):
    curr_time = datetime.datetime.now()
    if (curr_time - start_time).total_seconds() <= 70:
        try:
            resp_json = json.loads(resp.content)
            resp_key = resp_json['amp_response_key']
            red = redis.utils.Redis()
            red_val = json.loads(red.get(resp_key))
            resp_code = red_val['error']['code']
            resp_msg = red_val['error']['message']
            if resp_code is not None and resp_msg is not None:
                return resp_code, resp_msg
            else:
                return retry_get_data(resp, start_time)
        except:
            return retry_get_data(resp, start_time)
    else:
        raise ConnectionError('Redis value not found')


def retry_get_data(resp, start_time):
    print "waiting for redis..."
    time.sleep(5)
    return get_data(resp, start_time)


if __name__ == '__main__':
    with property_values(wps=5, api_url=API_URL):
        print "Testing error codes"
        test_error_codes()
