import lib.parse_requests as PR
import lib.check_svc_status as SVC
from lib.config_bootstrap_properties import property_values

import scripts.get_queue_info as Q
from lib.settings import AWS_ENV
from lib.settings import API_URL
from lib.settings import FB_API_VERSION

url = ('http://localhost:9090/%s/6017678624990'
       '?amp_act_id=act_345926612250332'
       '&amp_method=graph_update_adgroup'
       '&redownload=1'
       '&pause=True' % FB_API_VERSION)


def send_verify_request(priority, **kwargs):
    resp = PR.send_batch(**kwargs)
    # resp = PR.send_batch(extra_kwargs={'headers': p1_headers,
    #                                    'data': p1_data,
    #                                    'verify': False
    #                                    },
    #                      **kwargs
    #                      )
    queued = Q.check_items_in_queue()
    queue_priority = '%s_in_queue' % priority
    assert queued[queue_priority] > 1, (
        "Request did not appear in priority queue %s") % priority
    print "%s OK" % priority


def test_p1_queue():
    SVC.restart_fbproxy()
    kwargs = {'req_type': 'put', 'num_reqs': 50, 'url': url}
    send_verify_request('p1', **kwargs)


def test_p2_queue():
    SVC.restart_fbproxy()
    kwargs = {'req_type': 'delete', 'num_reqs': 10, 'url': url}
    send_verify_request('p2', **kwargs)


def test_p3_queue():
    SVC.restart_fbproxy()
    kwargs = {'req_type': 'post', 'num_reqs': 10, 'url': url}
    send_verify_request('p3', **kwargs)


# if __name__ == '__main__':
#     with property_values(wps=1, api_url=API_URL):
#         print "Testing prioritization"
#         test_p1_queue()
#         test_p2_queue()
#         test_p3_queue()





# p1_data = '{"filter":{"base":{"account_names":"Ampush_Careers","account_id":"2271","contract_id":"1392"},"json":{},"name":{"adgroup":{"adgroup_operator":"and","name":"","tag":"","tag_operator":"and"},"campaign":{"name":"","campaign_operator":"and"}}},"actions":{"update_adgroup_bid":{"value":"max(min(=bid+.02,5),-1)"}},"action_type":"adgroups","fbids":[6017653689390],"sdate":"20150617","edate":"20150618","name":null,"schedule":{"frequency":0,"metrics_range":0,"metrics_range_shift":0,"status":null}}'

# p1_headers = {'Connection': 'keep-alive',
#               'Content-Length': '491',
#               'Accept': 'application/json, text/javascript, */*; q=0.01',
#               'Origin': 'https://sb20-rep1.ampush1.com',
#               'X-Requested-With': 'XMLHttpRequest',
#               'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36',
#               'Content-Type': 'application/json;charset=UTF-8',
#               'Referer': 'https://sb20-rep1.ampush1.com/amp/manage/ads/',
#               'Accept-Encoding': 'gzip, deflate',
#               'Accept-Language': 'en-US,en;q=0.8',
#               'Cookie': 'sessionid=652145b7635b5bcab58bf25324b3ccad; currentContractID=1392; dateContractPicker=%7B%22start%22%3A%2220150617000000%22%2C%22end%22%3A%2220150618000000%22%7D; api_version=v2%2Fperformance; _gat=1; __utmt=1; __utma=96393822.2100969719.1434587191.1434587191.1434617518.2; __utmb=96393822.1.10.1434617518; __utmc=96393822; __utmz=96393822.1434587191.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); _ga=GA1.2.2100969719.1434587191; account_name_filter=Ampush_Careers; manager_account_filter=Ampush_Careers'
#               }

# p1_url = "https://%s/manager/api/submit" % AWS_ENV

# p2_data = '{"filter":{"base":{"account_names":"Ampush Ads","account_id":"1946","contract_id":"117"},"json":{"adgroup_status":{"type":"status","selected":"DISAPPROVED"}},"name":{"adgroup":{"adgroup_operator":"and","name":"","tag":"","tag_operator":"and"},"campaign":{"name":"","campaign_operator":"and"}}},"actions":{"delete_adgroup":{"value":"DELETED"}},"action_type":"adgroups","fbids":[6025201527862,6025203163862],"sdate":"20150618","edate":"20150625","name":null,"schedule":{"frequency":0,"metrics_range":0,"metrics_range_shift":0,"status":null}}'

# p2_headers = {'Cookie': 'currentContractID=568; sessionid=ae52b5068f50daf89d3a5d31d9345d49; __utmt=1; __utma=261920847.1180221116.1434758971.1435125095.1435130291.13; __utmb=261920847.1.10.1435130291; __utmc=261920847; __utmz=261920847.1434758971.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); _ga=GA1.2.260984452.1434758970; account_name_filter=Ampush%20Ads; manager_account_filter="Ampush Ads"; _gat=1',
#               'Origin': 'https://sb6-qa1.ampush1.com',
#               'Accept-Encoding': 'gzip, deflate',
#               'Accept-Language': 'en-US,en;q=0.8',
#               'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.130 Safari/537.36',
#               'Content-Type': 'application/json;charset=UTF-8',
#               'Accept': 'application/json, text/javascript, */*; q=0.01',
#               'Referer': 'https://sb6-qa1.ampush1.com/amp/manage/ads/',
#               'X-Requested-With': 'XMLHttpRequest',
#               'Connection': 'keep-alive'
#               }

# p2_url = "https://%s/manager/api/submit" % AWS_ENV

# p3_data = '------WebKitFormBoundaryM0mxcKYDxbyArBu0\r\nContent-Disposition: form-data; name="Account"\r\n\r\nact_1387427321516765\r\n------WebKitFormBoundaryM0mxcKYDxbyArBu0\r\nContent-Disposition: form-data; name="csvfile"; filename="Success - Website Conversions MAPAS.csv"\r\nContent-Type: text/csv\r\n\r\n\r\n------WebKitFormBoundaryM0mxcKYDxbyArBu0\r\nContent-Disposition: form-data; name="continue_upload"\r\n\r\nfalse\r\n------WebKitFormBoundaryM0mxcKYDxbyArBu0--\r\n'

# p3_headers = {'Cookie': 'sessionid=652145b7635b5bcab58bf25324b3ccad; __utma=96393822.2100969719.1434587191.1434587191.1434587191.1; __utmc=96393822; __utmz=96393822.1434587191.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); currentContractID=1392; dateContractPicker=%7B%22start%22%3A%2220150617000000%22%2C%22end%22%3A%2220150618000000%22%7D; api_version=v2%2Fperformance; account_name_filter=Ampush_Careers;manager_account_filter=Ampush_Careers; _ga=GA1.2.2100969719.1434587191', 
#               'Origin': 'https://sb20-rep1.ampush1.com',
#               'Accept-Encoding': 'gzip, deflate',
#               'Accept-Language': 'en-US,en;q=0.8',
#               'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36',
#               'Content-Type': 'multipart/form-data; boundary=----WebKitFormBoundaryM0mxcKYDxbyArBu0',
#               'Accept': '*/*',
#               'Referer': 'https://sb20-rep1.ampush1.com/amp/',
#               'X-Requested-With': 'XMLHttpRequest',
#               'Connection': 'keep-alive',
#               }

# p3_url = "https://%s.ampush1.com" % AWS_ENV