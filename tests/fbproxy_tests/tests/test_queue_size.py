from lib.config_bootstrap_properties import property_values
from lib.config_bootstrap_properties import ConfigBootstrapProperties as CBP  # noqa
import lib.parse_requests as PR
from lib.settings import API_URL
from lib.settings import FB_API_VERSION

url = ('http://localhost:9090/%s/6017678624990'
       '?amp_act_id=act_345926612250332'
       '&amp_method=graph_update_adgroup'
       '&redownload=1'
       '&pause=True' % FB_API_VERSION)


def verify_assertion_queue_size(function):
    def assertion_queue_size():
        function_name = function.func_name
        resp = function()
        print "testing %s ......" % function_name
        if 'fail' in function_name:
            result = test_negative_case(resp)
        else:
            result = test_positive_case(resp)
        print "OK"
        return result
    return assertion_queue_size


def test_positive_case(resp):
    qsize = CBP._read_value('qsize')
    freq = get_freq_pass_fail(resp)
    assert freq['fail'] == 0, (
        "All requests should be queued, however %s of %s "
        "failed due to queue size full" % (freq['fail'], len(resp)))
    return resp, freq, qsize, len(resp)


def test_negative_case(resp):
    qsize = CBP._read_value('qsize')
    freq = get_freq_pass_fail(resp)
    assert freq['fail'] == 1, (
        "1 request should fail due to queue size full, however %s of %s "
        "failed") % (freq['fail'], len(resp))
    return resp, freq, qsize, len(resp)


def get_freq_pass_fail(resps):
    freq = {'pass': 0, 'fail': 0}
    for resp in resps:
        if 'fail_queue_full' in resp.content:
            freq['fail'] += 1
        else:
            freq['pass'] += 1
    return freq


@verify_assertion_queue_size
def test_queue_pass_10():
    with property_values(qsize=10):
        kwargs = {'num_reqs': 10, 'url': url}
        resp = PR.send_batch(**kwargs)
    return resp


@verify_assertion_queue_size
def test_queue_pass_100():
    with property_values(qsize=100):
        kwargs = {'num_reqs': 100, 'url': url}
        resp = PR.send_batch(**kwargs)
    return resp


@verify_assertion_queue_size
def test_queue_pass_1000():
    with property_values(qsize=1000):
        kwargs = {'num_reqs': 1000, 'url': url}
        resp = PR.send_batch(**kwargs)
    return resp


@verify_assertion_queue_size
def test_queue_fail_100():
    with property_values(qsize=100):
        kwargs = {'num_reqs': 103, 'url': url}
        resp = PR.send_batch(**kwargs)
    return resp


@verify_assertion_queue_size
def test_queue_fail_10():
    with property_values(qsize=10):
        kwargs = {'num_reqs': 13, 'url': url}
        resp = PR.send_batch(**kwargs)
    return resp


@verify_assertion_queue_size
def test_queue_fail_1000():
    with property_values(qsize=1000):
        kwargs = {'num_reqs': 1003, 'url': url}
        resp = PR.send_batch(**kwargs)
    return resp

if __name__ == '__main__':
    with property_values(wps=2, api_url=API_URL):
        print "Testing queue size"
        test_queue_pass_10()
        test_queue_fail_10()
        test_queue_pass_100()
        test_queue_fail_100()
        test_queue_pass_1000()
        test_queue_fail_1000()
