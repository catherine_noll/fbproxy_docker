import redis

import get_queue_info as Q
import lib.check_svc_status as SVC


def check_items_in_redis():
    red = redis.utils.Redis()
    jobs = [key for key in red.keys()
            if 'CONFIG' not in key]
    return len(jobs)


def drain_queue():
    # redis_jobs = check_items_in_redis()
    queued = Q.check_items_in_queue()
    print queued
    assert int(queued['total_in_queue']) > 0, "No jobs submitted to queue"
    SVC.restart_fbproxy()
    drained_queue = Q.check_items_in_queue()
    print drained_queue
    # redis_jobs_after_restart = check_items_in_redis()
    assert int(drained_queue['total_in_queue']) == 0, (
        "Queue should be empty but is %s") % drained_queue
    # Keys in redis that are associated with FB Proxy jobs
    # aren't drained after restart

if __name__ == '__main__':
    drain_queue()
