import json
import requests
from StringIO import StringIO

import lib.parse_requests as PR
from lib.settings import AWS_ENV
from lib.settings import FBPROXY_LOGPATH
from lib.settings import FBPROXY_PORT


def get_num_in_queue(file_obj):
    while True:
        line = file_obj.next().strip()
        if "Total Requests In Progress" in line:
            total_in_progress = line.split(': ')[-1]
            p1_in_progress = file_obj.next().strip().split(': ')[-1]
            p2_in_progress = file_obj.next().strip().split(': ')[-1]
            p3_in_progress = file_obj.next().strip().split(': ')[-1]
            p4_in_progress = file_obj.next().strip().split(': ')[-1]
        if "Total Queued Requests" in line:
            total_in_queue = line.split(': ')[-1]
            p1_in_queue = file_obj.next().strip().split(': ')[-1]
            p2_in_queue = file_obj.next().strip().split(': ')[-1]
            p3_in_queue = file_obj.next().strip().split(': ')[-1]
            p4_in_queue = file_obj.next().strip().split(': ')[-1]
            return {'total_in_queue': total_in_queue,
                    'p1_in_queue': p1_in_queue,
                    'p2_in_queue': p2_in_queue,
                    'p3_in_queue': p3_in_queue,
                    'p4_in_queue': p4_in_queue,
                    'total_in_progress': total_in_progress,
                    'p1_in_progress': p1_in_progress,
                    'p2_in_progress': p2_in_progress,
                    'p3_in_progress': p3_in_progress,
                    'p4_in_progress': p4_in_progress
                    }


def check_items_in_queue(host=AWS_ENV, port=FBPROXY_PORT):
    mod_time = PR.modification_time(FBPROXY_LOGPATH)
    PR.wait_for_log_start(mod_time, FBPROXY_LOGPATH)
    url = 'http://%s:%s/hb?sys=1' % (host, port)
    req = requests.get(url)
    file_obj = StringIO(req.content)
    return get_num_in_queue(file_obj)


if __name__ == '__main__':
    print json.dumps(check_items_in_queue())
