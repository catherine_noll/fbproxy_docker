#!/bin/bash

# Ampush_Careers account_id = 2271
# Ampush Recruiting contract_id 1392
# pl_contractaccountmap Row 2735

mysql -h db1-qa1 -u ampush_db1 -pampush_db1 -D ampush_db1 -e "\
UPDATE pl_contractaccountmap
SET edate=DATE_ADD(CURDATE(), INTERVAL 1 YEAR)
WHERE id=2735;\
"


mysql -h db1-qa1 -u ampush_db1 -pampush_db1 -D ampush_db1 -e "\
SELECT edate
FROM pl_contractaccountmap
WHERE account_id=2271
AND contract_id=1392;\
"