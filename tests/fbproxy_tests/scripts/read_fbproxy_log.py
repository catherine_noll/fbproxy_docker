import json
import lib.parse_requests as PR

from lib.settings import FBPROXY_LOGPATH


def read_fbproxy_log():
    # fbproxy_log = PR.setup_log(FBPROXY_LOGPATH)
    file_object = open(FBPROXY_LOGPATH, 'r')
    return PR.parse_fbproxy_log(1, file_object)


if __name__ == '__main__':
    print json.dumps(read_fbproxy_log())

 # = subprocess.call(cmd, stdout=subprocess.PIPE)
