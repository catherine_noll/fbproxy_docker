#!/bin/bash

# Ampush_Careers account_id = 2271
# Ampush Recruiting contract_id 1392

mysql -h db1-qa1 -u ampush_db1 -pampush_db1 -D ampush_db1 -e "\
UPDATE pl_contractaccountmap pl
INNER JOIN facebook_account fa
ON pl.account_id = fa.id
SET fa.access_token=REPLACE(fa.access_token, '_', '')
WHERE pl.account_id=2271
AND pl.edate>CURDATE();\
"


mysql -h db1-qa1 -u ampush_db1 -pampush_db1 -D ampush_db1 -e "\
SELECT fa.access_token 
FROM pl_contractaccountmap pl
INNER JOIN facebook_account fa
ON pl.account_id = fa.id
WHERE pl.account_id=2271
AND pl.edate>CURDATE();\
"
